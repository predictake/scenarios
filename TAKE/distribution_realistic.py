#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = "Yann Maret"
__copyright__ = "Copyright 2019, HEIA-FR - PredicTAKE project"
__date__ = "2022-12-02"

import json
import math
import operator
import random
import sys
import copy


class Message:
    source = 0
    recipients = list()
    size = 100
    time = 0

    def __init__(self, time, source, recipients, size):
        self.source = source
        self.recipients = recipients
        self.size = size
        self.time = time

    def toCSVString(self):
        recip = []
        s = self.source + 1
        for r in self.recipients:
            recip.append(str(r + 1))

        return "{},{},{},{}\n".format(self.time, s, "-".join(recip), self.size)

    def __str__(self):
        return "Message = [" + str(self.time) + ", " + str(self.source) + ", " + str(self.recipients) + ", " + str(
            self.size) + "]"


class Distribution:
    """
    Script to generate TAKE messaging distribution files in JSON format.
    See examples in main() for script usage.
    """

    nb_of_nodes = 0
    message_second = 0
    duration = 0
    nb_messages = 0
    messages = list()
    message_sizes = [100]
    multi_recipients = False

    def __init__(self, nb_of_nodes, duration, message_second, split_src=False, messages=[], message_sizes=[100],
                 multi_recipients=False):
        # The number of nodes in the simulation
        self.nb_of_nodes = nb_of_nodes
        # The number of messages per second (from each node)
        self.message_second = message_second
        # The duration of the simulation
        self.duration = duration
        # specify a source node for pratical test as a dedicated instance of take with the traffic runs on nodes
        self.split_src = split_src
        # Total message per nodes floored
        self.mess_per_node = int(math.floor(message_second * duration))  # msg/s/src -> msg/src
        # The total number of messages sent during the simulation
        self.nb_messages = nb_of_nodes * self.mess_per_node  # total number of message
        # The list of messages to be sent
        self.messages = messages
        # The potential sizes of messages. If more than one size are provided,
        # each message will use a randomly selected size
        self.message_sizes = message_sizes
        # Use multi-recipients ? (the recipient(s) are randomly selected)
        self.multi_recipients = multi_recipients

    def get_messages(self, node_ID):
        node_messages = []
        for msg in self.messages:
            if str(msg.source) == str(node_ID):
                node_messages.append(msg)
        return node_messages

    @staticmethod
    def create(nb_of_nodes, duration, message_second_src, split_src=False, messages=[], message_sizes=[100],
               multi_recipients=False):
        distribution = Distribution(
            nb_of_nodes, duration, message_second_src, split_src, messages, message_sizes, multi_recipients)
        distribution.generate()
        return distribution

    @staticmethod
    def load(filename):
        # Load from file
        # Set values and load messages
        with open(filename, 'r') as outfile:
            data = json.load(outfile)
            msg_sizes = [i for i in data['message_sizes']]
            myDistrib = Distribution(nb_of_nodes=data['nodes'], message_second=data['message_second'], duration=data[
                'duration'], message_sizes=msg_sizes, multi_recipients=data['multi_recipients'])
            for message in data['messages']:
                recipients = [i for i in message['recipients']]
                myDistrib.messages.append(Message(time=message['time'], source=message[
                    'source'], recipients=recipients, size=message['size']))
            return myDistrib

    def save(self, folder="measures/", filename=""):
        if filename is "":
            if self.multi_recipients:
                recipient_string = "multi-recipients"
            else:
                recipient_string = "mono-recipients"
            filename = "distribution-deterministic_{0}-Nodes_{1}s_{2}-messages_{3}-bytes_{4}_real.json".format(
                self.nb_of_nodes, self.duration, int(self.nb_messages),
                "[" + ",".join(map(str, self.message_sizes)) + "]", recipient_string)

        if self.split_src:

            tmpmesgs = self.messages  # make a copy
            persrc = {}
            for tmpmsg in tmpmesgs:
                if tmpmsg.source not in persrc.keys():
                    persrc[tmpmsg.source] = []
                persrc[tmpmsg.source].append(tmpmsg)

            for src, msgs in persrc.items():
                self.messages = msgs

                # self.messages.sort(key=operator.attrgetter('time'))

                filename = str(src)+"/distribution-deterministic_{0}-Nodes_{1}s_{2}-totalmessages_{3}_messagesrc_{4}-bytes_{5}_A2A.json".format(
                    self.nb_of_nodes, self.duration, int(self.nb_messages), len(self.messages),
                    "[" + ",".join(map(str, self.message_sizes)) + "]", recipient_string)

                with open(folder + filename, 'w') as outfile:
                    json.dump(self, fp=outfile, default=self.jdefault)
                    print("Distribution saved to <{0}>".format(filename))

        else:
            with open(folder + filename, 'w') as outfile:
                json.dump(self, fp=outfile, default=self.jdefault)
                print("Distribution saved to <{0}>".format(filename))

    def generate(self):
        self.messages = []
        random.seed(42)  # use a fixed random seed
        total_messages = 0

        #pair_burst_time = 20  # duration of a traffic burst in second
        #min_pair_burst_time = 10
        #max_number_packet_burst = 15  # number of packet max per burst
        #min_number_packet_burst = 10
        #pair_burst_number = 10  # number of consecutive burst to generate for a pair
        #min_pair_burst_number = 5
        #inter_burst_time = 4  # duration of the inter arrival burst in second
        #min_inter_burst_time = 1
        #interfactor = 1

        pair_burst_time = 1  # duration of a traffic burst in second
        min_pair_burst_time = 1
        max_number_packet_burst = 1  # number of packet max per burst
        min_number_packet_burst = 1
        pair_burst_number = 1  # number of consecutive burst to generate for a pair
        min_pair_burst_number = 1
        inter_burst_time = 1  # duration of the inter arrival burst in second
        min_inter_burst_time = 1
        interfactor = 1

        for source in range(self.nb_of_nodes):
            maxmessage = False
            total_source_message = 0

            times = []
            for i in range(20):
                times.append(random.randint(1, self.duration - pair_burst_time))

            for i in range(self.mess_per_node):

                potential_recipients = list(range(self.nb_of_nodes))
                potential_recipients.remove(source)  # Avoid sending to himself
                recipients = [random.choice(potential_recipients)]  # pick only one destination

                # ctime = random.randint(1,
                #                       self.duration - pair_burst_time)  # make a not so random choice of the time to avoid multiple recipient at the same time
                ctime = random.choice(times)
                times.remove(ctime)
                if len(times) == 0:  # make sure to keep times available
                    times.append(random.randint(1, self.duration - pair_burst_time))
                    # print("request more time")

                sequence = random.randint(min_pair_burst_number, pair_burst_number)

                sinter = 0
                for s in range(sequence):

                    dtime = random.randint(min_pair_burst_time, pair_burst_time)  # duration of the burst
                    sinter += random.randint(min_inter_burst_time,
                                             inter_burst_time) / interfactor  # inter burst (interpacket) arrival time
                    mpacket = random.randint(min_number_packet_burst,
                                             max_number_packet_burst)  # number of packet per burst

                    size = self.message_sizes[0]  # choice randomly a packet size from the list

                    for p in range(mpacket):
                        time = round(ctime + sinter + dtime * p / mpacket, 2)

                        msg = Message(time=time, source=source, recipients=recipients, size=size)
                        self.messages.append(msg)
                        total_source_message += 1

                        if total_source_message >= self.mess_per_node:
                            maxmessage = True
                            break
                    sinter += dtime
                    if maxmessage:
                        break
                if maxmessage:
                    break
                # if total_source_message >= self.mess_per_node:
                #    break

        self.messages.sort(key=operator.attrgetter('time'))
        print("The generated distribution contains a total of {0} messages".format(total_messages))

    def export_to_csv(self, folder="measures/", filename=""):
        if filename is "":
            if self.multi_recipients:
                recipient_string = "multi-recipients"
            else:
                recipient_string = "mono-recipients"
            filename = "distribution-deterministic_{0}-Nodes_{1}s_{2}-messages_{3}-bytes_{4}_real.csv".format(
                self.nb_of_nodes, self.duration, int(self.nb_messages),
                "[" + ",".join(map(str, self.message_sizes)) + "]", recipient_string)

        ordered_messages = sorted(self.messages, key=lambda x: x.time, reverse=False)

        with open(folder + filename, 'w') as outfile:
            outfile.write("time,source,recipient(s),message\n")
            for message in ordered_messages:
                outfile.write(message.toCSVString())

    @staticmethod
    def jdefault(o):
        return o.__dict__


def main(argv):
    ## SAMPLES ####################
    #
    # 1 Save and load a distribution with a fixed name
    # myDistrib = Distribution.create(nb_of_nodes=4, duration=50, message_second=1, multi_recipients=True, message_sizes=[100])
    # myDistrib.save("sample_distribution.json")
    # myLoadedDistrib = Distribution.load('sample_distribution.json')
    # print myLoadedDistrib.messages[0]
    #
    # 2 Save a distribution with automatic naming
    # myDistrib = Distribution.create(nb_of_nodes=4, duration=50, message_second=1, multi_recipients=True, message_sizes=[100])
    # myDistrib.save()
    ##############################

    # myLoadedDistrib = Distribution.load('dd_24-Nodes_1002s_24048-messages_[100]-bytes_mono-recipients.json')
    # myLoadedDistrib.split_src = True
    # myLoadedDistrib.nb_of_nodes= 24
    # myLoadedDistrib.duration = 1002
    # myLoadedDistrib.nb_messages = 24048
    # myLoadedDistrib.messages = 0
    # myLoadedDistrib.save()

    # exit()

    # for i in range(1, 12):
    myDistrib = Distribution.create(nb_of_nodes=6, split_src=False, duration=900, message_second_src=4,
                                    multi_recipients=False, message_sizes=[100])
    myDistrib.export_to_csv()
    myDistrib.save()


if __name__ == "__main__":
    main(sys.argv[1:])
