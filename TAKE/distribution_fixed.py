#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = "Simon Ruffieux"
__copyright__ = "Copyright 2019, HEIA-FR - PredicTAKE project"
__date__ = "2019-08-27"

import json
import math
import operator
import random
import sys


class Message:
    source = 0
    recipients = list()
    size = 100
    time = 0

    def __init__(self, time, source, recipients, size):
        self.source = source
        self.recipients = recipients
        self.size = size
        self.time = time

    def __str__(self):
        return "Message = [" + str(self.time) + ", " + str(self.source) + ", " + str(self.recipients) + ", " + str(self.size) + "]"


class Distribution:
    """
    Script to generate TAKE messaging distribution files for FIXED RECIPIENTS in JSON format.
    See examples in main() for script usage.
    """

    nb_of_nodes = 0
    message_second = 0
    duration = 0
    nb_messages = 0
    messages = list()
    message_sizes = [100]
    multi_recipients = False

    def __init__(self, nb_of_nodes, duration, message_second, messages=[], message_sizes=[100], multi_recipients=False):
        # The number of nodes in the simulation
        self.nb_of_nodes = nb_of_nodes
        # The number of messages per second (from each node)
        self.message_second = message_second
        # The duration of the simulation
        self.duration = duration
        # Total messge per nodes floored
        self.mess_per_node = int(math.floor(message_second * duration))
        # The total number of messages sent during the simulation
        self.nb_messages = nb_of_nodes * self.mess_per_node
        # The list of messages to be sent
        self.messages = messages
        # The potential sizes of messages. If more than one size are provided,
        # each mesage will use a randomly selected size
        self.message_sizes = message_sizes
        # Use multi-recipients ? (the recipient(s) are randomly selected)
        self.multi_recipients = multi_recipients

    def get_messages(self, node_ID):
        node_messages = []
        for msg in self.messages:
            if str(msg.source) == str(node_ID):
                node_messages.append(msg)
        return node_messages

    @staticmethod
    def create(nb_of_nodes, duration, message_second, messages=[], message_sizes=[100], multi_recipients=False):
        distribution = Distribution(
            nb_of_nodes, duration, message_second, messages, message_sizes, multi_recipients)
        distribution.generate()
        return distribution

    @staticmethod
    def load(filename):
        # Load from file
        # Set values and load messages
        with open(filename, 'r') as outfile:
            data = json.load(outfile)
            msg_sizes = [i for i in data['message_sizes']]
            myDistrib = Distribution(nb_of_nodes=data['nodes'],  message_second=data['message_second'], duration=data[
                                     'duration'], message_sizes=msg_sizes, multi_recipients=data['multi_recipients'])
            for message in data['messages']:
                recipients = [i for i in message['recipients']]
                myDistrib.messages.append(Message(time=message['time'], source=message[
                                          'source'], recipients=recipients, size=message['size']))
            return myDistrib

    def save(self, filename=""):
        if filename is "":
            if self.multi_recipients:
                recipient_string = "multi-recipients"
            else:
                recipient_string = "mono-recipients"
            filename = "distribution-deterministic_{0}-Nodes_{1}s_{2}-messages_{3}-bytes_{4}_FIXED.json".format(self.nb_of_nodes, self.duration, int(self.nb_messages), "[" + ",".join(map(str,self.message_sizes))+ "]" , recipient_string)
        with open("./"+filename, 'w') as outfile:
            json.dump(self, fp=outfile, default=self.jdefault)
            print "Distribution saved to <{0}>".format(filename)

    def generate(self):
        self.messages = []
        total_messages = 0
        nb_recipients = 1
        for source in [1,5]: # sources 2, 6
            total_source_message = 0
            for i in range(self.mess_per_node):
                size = self.message_sizes[0]
                for time in range(1, self.duration):
                    recipients = [4] if source is 1 else [8] # nodes 5 and 9  range(0, self.nb_of_nodes)
                    msg = Message(time=time, source=source, recipients=recipients, size=size)
                    self.messages.append(msg)

                    total_source_message += nb_recipients
                    total_messages += nb_recipients
                if total_source_message >= self.mess_per_node:
                    break
            if total_messages >= self.nb_messages:
                break
        self.messages.sort(key=operator.attrgetter('time'))
        print "The generated distribution contains a total of {0} messages".format(total_messages)

    @staticmethod
    def jdefault(o):
        return o.__dict__


def main(argv):

    ## SAMPLES ####################
    #
    # 1 Save and load a distribution with a fixed name
    # myDistrib = Distribution.create(nb_of_nodes=4, duration=50, message_second=1, multi_recipients=True, message_sizes=[100])
    # myDistrib.save("sample_distribution.json")
    # myLoadedDistrib = Distribution.load('sample_distribution.json')
    # print myLoadedDistrib.messages[0]
    #
    # 2 Save a distribution with automatic naming
    # myDistrib = Distribution.create(nb_of_nodes=4, duration=50, message_second=1, multi_recipients=True, message_sizes=[100])
    # myDistrib.save()
    ##############################

    myDistrib = Distribution.create(nb_of_nodes=9, duration=300, message_second=56, multi_recipients=False, message_sizes=[100])
    myDistrib.save()


if __name__ == "__main__":
    main(sys.argv[1:])
