from eelfile import write_eel_file
import sys
import random
import math

from eelfile import write_eel_file
sys.path.append('../')


def quickanddurtyshiftcoordinates1(x, y):
    # https://gis.stackexchange.com/questions/2951/algorithm-for-offsetting-a-latitude-longitude-by-some-amount-of-meters
    lat = 51.0
    lon = 0.0

    R = 6378137

    # offsets in meters
    dn = x
    de = y

    dLat = dn / R
    dLon = de / (R * math.cos(math.pi * lat / 180))

    latO = lat + dLat * 180 / math.pi
    lonO = lon + dLon * 180 / math.pi

    return latO, lonO


def quickanddurtyshiftcoordinates(x, y):
    lat = y / 111111.
    lon = x / (111111. * math.cos(lat))
    return lat, lon


def static(seed=1, exp=False, schedule=[]):
    dim = 600  # m 600 domain
    nodes = 24  # nodes count
    pl_limit = 117  # dB doesn't matter look at lo variable
    urban = 40  # alpha
    d_limit_m = 200  # max distance at which the signal is still good

    # internal data
    time = 1
    xynodes = []
    lonnodes = []
    latnodes = []
    random.seed(seed)
    lo = pl_limit - urban * math.log(d_limit_m, 10)
    lo = 25

    # output data
    eelinfo = {'pathloss': [], 'location': []}

    for n in range(nodes):
        x = random.randint(0, dim)
        y = random.randint(0, dim)
        xynodes.append((x, y))
        lat, lon = quickanddurtyshiftcoordinates(x, y)
        latnodes.append(lat)
        lonnodes.append(lon)
        alt = 0.0
        # if time not in eelinfo['location']:
        print(n + 1, lat, lon)
        eelinfo['location'].append([time, n + 1, lat, lon, alt])

    if exp:
        print(lonnodes)
        print(latnodes)
        exit()

    for i, ns in enumerate(xynodes):
        pathloss = []
        for j, nd in enumerate(xynodes[i + 1:]):
            j += i + 1
            dx = abs(ns[0] - nd[0])
            dy = abs(ns[1] - nd[1])
            d_m = math.sqrt(dx * dx + dy * dy)

            if d_m == 0:
                pl = 40
            else:
                pl = lo + urban * math.log(d_m, 10)

            if pl >= 120 and pl <= 122:
                print(i+1, j+1, pl)

            pathloss.append([j + 1, pl])
            # print i + 1, j + 1, d_m, pl

        if len(pathloss) >= 1:
            eelinfo['pathloss'].append([time, i + 1, pathloss])

    write_eel_file('generated_' + str(seed) + '_step.eel', eelinfo, schedule)


if __name__ == '__main__':

    for i in range(500):
        print(
            '{'+'"source": 0, "time": {}, "recipients": [1], "size": 100'.format(i)+'},')

    for i in [1]:
        schedule = []
        for j in range(0, 100):
            schedule.append(
                "{0}  nem:1 schedule schedules_urban_step{1}/scheduler_{2}.xml".format(j + 1, i, j))

        # static(i,schedule=schedule)
        static(i)
