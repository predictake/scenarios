import requests
import json

from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import shortest_path
from scipy.special import softmax

import sys

import xml.etree.ElementTree as ET
import itertools

import random

random.seed(0)


def _extract_graph(graphjson, nbnode=6):
    links = graphjson["collection"][0]["links"]

    connectivity_matrix = [[0] * nbnode for _ in range(nbnode)]
    DAT_matrix = [[0] * nbnode for _ in range(nbnode)]

    DAT = []

    for link in links:
        src = int(link["source"].split(".")[-1])
        tar = int(link["target"].split(".")[-1])
        cost = link["cost"]
        cost_text = link["cost_text"]
        # in_cost = link["properties"]["in_cost"]
        # in_text = link["properties"]["in_text"]

        connectivity_matrix[src - 1][tar - 1] = 1
        # connectivity_matrix[tar - 1][src - 1] = 1
        DAT_matrix[src - 1][tar - 1] = cost

        DAT.append((src, tar, cost))

        # print (cost)

    return connectivity_matrix, DAT_matrix, DAT


def _extract_route(graphjson):
    routes = graphjson["collection"][0]["route"]
    # {'destination': '10.100.0.2', 'next': '-', 'device': 'wlx60e32710b8c6', 'cost': 11927296, 'cost_text': '180bit/s (1 hops)', 'properties': {'destination_id': 'id_10.100.0.2', 'next_router_id': 'id_10.100.0.2', 'next_router_addr': '10.100.0.2', 'hops': 1, 'last_router_id': 'id_10.100.0.1', 'last_router_addr': '10.100.0.1'}}
    rt = []
    for route in routes:
        d = route['destination'].split(".")[-1]
        c = route['cost']
        nh = route['properties']['next_router_addr'].split(".")[-1]
        hop = route['properties']['hops']
        rt.append((d, nh, c, hop))

    return rt


def _extract_2hops(graphjson):
    twohoplinks = graphjson["link_twohop"]

    DAT_matrix_2hop = [[0] * nbnode for _ in range(nbnode)]
    for link in twohoplinks:
        src = int(link["link_bindto"].split(".")[-1])
        tar = int(link["twohop_address"].split(".")[-1])
        cost = link["domain_metric_out_raw"]

        DAT_matrix_2hop[src - 1][tar - 1] = cost

    return DAT_matrix_2hop


def _extract_MPR(graphjson):
    neighbors = graphjson["neighbor"]
    for neighbor in neighbors:
        if neighbor["domain_mpr_local"] == "true":
            return True

    return False


from datetime import datetime, timedelta
import pytz

folder_root = "X:/Results/laptop/experiments/results_all_laptops/bk_measure/LaptopShow-6nodesolsrtakeA2A1msg/20230824-1500/log/"
routefile = open(folder_root + "route.csv", "w")
routefile.write("time,nodei,dest,gw,cost\n")

csvfile = open(folder_root + "nvg.csv", "w")
csvfile.write("time,nodei,s_dat,t_dat,dat\n")


def olsr_graph_offline_log(nodeid, folder_root=folder_root, csvfile=csvfile, routefile=routefile, hopfile=None):
    dt = "20230308-1630"
    dt = "20230322-1015"
    dt = "20230607-1700"
    folder = "X:/Results/laptop/experiments/results_all_laptops/Laptop-6nodesolsrmgenTwoRatesCompare_Trip1/" + dt + "/log/{}/".format(
        nodeid)
    folder = folder_root + "/{}/".format(
        nodeid)

    # csvfile = open(folder + "dat.csv", "w")
    # csvfile.write("time,s_dat,t_dat,dat\n")

    # routefile = open(folder + "route.csv", "w")
    # routefile.write("time,nodei,dest,gw\n")
    #to_timezone = pytz.timezone("Europe/Zurich")

    with open(folder + "nodeviewgraph.log") as f:
        lines = f.readlines()
        time = 0
        netcolindx = 0
        for i, line in enumerate(lines):
            d = line.split(" ")
            if len(d) > 1:
                try:
                    timet = float(d[0]) #-2*3600
                    #timet = datetime.fromtimestamp(timet,to_timezone)
                    #timet2 = timet.timestamp()
                except:
                    pass
                try:
                    networkcollection = json.loads(" ".join(d[1:]))
                    if "NetworkGraph" in line:
                        con, datm, dat = _extract_graph(networkcollection)

                        if timet != 0:
                            td = timedelta(seconds=timet)
                            # ts = datetime.datetime.strptime(" ".join(data[0:2]), '%Y-%m-%d %H:%M:%S.%f').timestamp()
                            # h = td.seconds / 3600
                            # m = (h - int(h)) * 60
                            # s = (m - int(m)) *60
                            # t = str(int(h)) + ":" + str(int(m)) + ":" + str(int(s)) + "." + str(td.microseconds)

                            for d in dat:
                                i, j, c = d
                                csvfile.write("{},{},{},{},{},{}\n".format(0,timet, nodeid, i, j, c))

                            # for si in range(len(dat)):
                            #    dd = dat[si]
                            #    for tj in range(len(dd)):
                            #        # print(str(td).split(", ")[1], si + 1, tj + 1, dd[tj])
                            #        csvfile.write(
                            #            "{},{},{},{}\n".format(timet, si + 1, tj + 1, dd[tj]))
                    else:
                        rt = _extract_route(networkcollection)

                        td = timedelta(seconds=timet)

                        for r in rt:
                            d, nh, c, h = r
                            routefile.write("{},{},{},{}\n".format(timet, nodeid, d, nh))
                            hopfile.write("{},{},{},{}\n".format(timet, nodeid, d, h))

                except Exception as e:
                    print(e)
                    # pass
    # csvfile.flush()
    # csvfile.close()


#for i in range(1, 6 + 1):
#    olsr_graph_offline_log(i)
#exit()


def olsr_graph_offline(nodeid):
    with open("nodeviewgraph.json", "r") as f:
        graphjson = json.load(f)

    return _extract_graph(graphjson)


def olsr_graph_online(nodeid):
    getgraphcmd = "http://10.99.0.{}:2006/telnet/netjsoninfo graph".format(nodeid)

    r = requests.get(getgraphcmd)
    graphjson = json.loads(r.content.decode("utf-8"))
    # print(graphjson)

    # os.system(getgraphcmd) #for wget
    return _extract_graph(graphjson)


def olsr_link_2hops_offline(nodeid):
    with open("link2hop.json", "r") as f:
        graphjson = json.load(f)

    return _extract_2hops(graphjson)


def olsr_link_2hops_online(nodeid):
    getgraphcmd = "http://10.99.0.{}:2006/telnet/nhdpinfo json link_twohop".format(nodeid)

    r = requests.get(getgraphcmd)
    graphjson = json.loads(r.content.decode("utf-8"))
    # print(graphjson)

    return _extract_2hops(graphjson)


def olsr_node_isMPR_online(nodeid):
    getgraphcmd = "http://10.99.0.{}:2006/telnet/nhdpinfo json neighbor".format(nodeid)

    r = requests.get(getgraphcmd)
    graphjson = json.loads(r.content.decode("utf-8"))
    # print(graphjson)

    return _extract_MPR(graphjson)


def olsr_node_isMPR_offline(nodeid):
    with open("mpr.json", "r") as f:
        graphjson = json.load(f)

    return _extract_MPR(graphjson)


def scenario_get_position_EMANE(nodeid):
    """
    get the position from the GPS device
    :param nodeid:
    :return:
    """
    pass


def scenario_get_linkquality_EMANE(nodeid):
    """
    get the sinr value from EMANE
    :param nodeid:
    :return:
    """
    pass


def sceanrio_get_linkquality_laptop(nodeid):
    """
    get the RSSI from the monitoring interface on laptop
    :param nodeid:
    :return:
    """
    pass


def path_to_schedule(paths):
    schedule = []
    for dst, path in paths.items():
        if len(path) > 1:
            schedule += [i + 1 for i in path[:-1]]
    return schedule


def indent(elem, level=0):
    i = "\n" + level * "  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indent(elem, level + 1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i


def write_scheduler_reuse(node, schedule, file, management=True):
    # if not os.path.exists(folder):
    #    os.makedirs(folder)

    emane = ET.Element("emane-tdma-schedule")

    structure = ET.SubElement(emane, "structure")
    structure.set("frames", "1")
    if management:
        structure.set("slots", str(len(schedule[0]) + node))
        index = node
    else:
        structure.set("slots", str(len(schedule[0])))
        index = 0
    structure.set("slotoverhead", "30")
    structure.set("slotduration", "5800")
    structure.set("bandwidth", "250K")

    multiframe = ET.SubElement(emane, "multiframe")
    multiframe.set("frequency", "300M")
    multiframe.set("power", "47.0")
    multiframe.set("datarate", "1M")

    # management frame:
    frame = ET.SubElement(multiframe, "frame")
    frame.set("index", "0")
    n = list(range(1, node + 1))
    if management:
        for i, s in enumerate(n):
            man_slot = ET.SubElement(frame, "slot")
            man_slot.set("index", str(i))
            man_slot.set("nodes", str(s))
            tx = ET.SubElement(man_slot, "tx")
            tx.set("class", "3")

    # find not transmitting node to set rx
    rx = list(range(1, node + 1))  # get all nodes
    flatschedule = schedule
    flatschedule = list(itertools.chain(*flatschedule))
    flatschedule = list(itertools.chain(*flatschedule))

    for nn in flatschedule:
        try:
            rx.remove(nn)  # remove the nodes that transmit in the first slots
        except ValueError:
            print("missing", nn)
    for rxn in rx:  # make all other nodes rx
        slot = ET.SubElement(frame, "slot")
        slot.set("index", str(0))
        slot.set("nodes", str(rxn))
        ET.SubElement(slot, "rx")

    for i, s in enumerate(schedule):  # [frames[slots]]
        for j, n in enumerate(s):  # slot[node]
            man_slot = ET.SubElement(frame, "slot")
            man_slot.set("index", str(index))
            man_slot.set("nodes", ",".join(str(a) for a in n))
            tx = ET.SubElement(man_slot, "tx")
            tx.set("class", "0")
            index += 1

    indent(emane)
    tree = ET.ElementTree(emane)
    tree.write(file, encoding='utf-8', method="xml")


def printmatlab(arr):
    print("[")
    for a in arr:
        print(a, ";")
    print("]")


def printdataset(datgraphs):
    # request 5 arr...
    # norm for the min and inverse since the DAT, leave 0 as it is
    # complet arr with easy link

    for arrs in datgraphs:  # all nodes available

        mindat = None
        maxdat = None
        for arr in arrs:
            for i, a in enumerate(arr):
                for j, dat in enumerate(a):
                    if dat == 0:
                        continue
                    if mindat is None:
                        mindat = dat
                    elif mindat > dat:
                        mindat = dat

                    if maxdat is None:
                        maxdat = dat
                    elif maxdat < dat:
                        maxdat = dat
        adjs = []
        # nnode = 24
        node_features = ["0"] * nbnode
        node_features[1] = "1"
        node_features[12] = "1"
        nedge = 0
        edge_features = []
        link = "1_12"
        label = 1

        for it, arr in enumerate(arrs):
            for i, a in enumerate(arr):
                for j, dat in enumerate(a):
                    if j > i:
                        if dat == 0:
                            continue

                        if "{}_{}".format(i, j) not in adjs:
                            adjs.append("{}_{}".format(i, j))
                            edge_features.append([])

                        idx = adjs.index("{}_{}".format(i, j))
                        # take the average of both direction?
                        dat2 = arrs[it][j][i]
                        edge_features[idx].append(str(round(1 - (dat - mindat) / maxdat, 2)))

            for ef in range(len(edge_features)):
                while len(edge_features[ef]) != it + 1:
                    edge_features[ef].append(str(0))

        # a = -softmax(edge_features)

        # pick a random linkij
        i = random.randint(nbnode)
        j = random.randint(nbnode)

        # get the vlaue of the link on the last graph on node i and test on node j
        if datgraphs[-1][i][j] == 0:
            label = 0
        link = "{}_{}".format(i, j)

        node_features = ["0"] * nbnode
        node_features[i] = "1"
        node_features[j] = "1"

        # print("adj,nnode,node_features,nedge,edge_features,link,label")
        print(
            "{},{},{},{},{},{},{}".format("|".join(adjs), str(nbnode), "|".join(node_features), len(edge_features),
                                          "|".join(["_".join(nf) for nf in edge_features]), link,
                                          label))


def customdataset():
    src = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4,
           4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9, 10, 10,
           11, 13, 13, 13, 14, 16, 16, 16, 16, 16, 16, 16, 16, 17, 17, 17, 17, 17, 17, 17, 18, 18, 18, 18, 18, 18, 19,
           19, 19, 19, 19, 20, 20, 20, 20, 21, 21, 21, 22, 22, 23]
    tar = [2, 3, 16, 17, 18, 19, 20, 21, 22, 23, 24, 13, 16, 17, 18, 19, 20, 21, 22, 23, 24, 4, 5, 6, 7, 8, 9, 10, 11,
           12, 16, 18, 5, 6, 7, 8, 9, 10, 11, 12, 6, 7, 8, 9, 10, 11, 12, 16, 7, 8, 9, 10, 11, 12, 8, 9, 10, 11, 12, 16,
           9, 10, 11, 12, 10, 11, 12, 16, 17, 18, 11, 12, 12, 14, 15, 20, 15, 17, 18, 19, 20, 21, 22, 23, 24, 18, 19,
           20, 21, 22, 23, 24, 19, 20, 21, 22, 23, 24, 20, 21, 22, 23, 24, 21, 22, 23, 24, 22, 23, 24, 23, 24, 24]

    for s, t in zip(src, tar):
        s = s - 1
        t = t - 1
        node_features = ["0"] * nnode
        node_features[s] = "1"
        node_features[t] = "1"
        link = "{}_{}".format(s, t)
        label = "1"

        print(
            "{},{},{},{},{},{},{}".format("|".join(adjs), str(nnode), "|".join(node_features), len(edge_features),
                                          "|".join(["_".join(nf) for nf in edge_features]), link,
                                          label))

    src = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4,
           4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7,
           7, 7, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9, 9, 9, 9, 10, 10, 10, 10, 10,
           10, 10, 10, 10, 10, 10, 10, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 12, 12, 12, 12, 12, 12, 12, 12,
           12, 12, 12, 12, 13, 13, 13, 13, 13, 13, 13, 13, 14, 14, 14, 14, 14, 14, 14, 14, 14, 15, 15, 15, 15, 15, 15,
           15, 15, 15]
    tar = [4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 13, 14, 15, 17, 19, 20,
           21, 22, 23, 24, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 13, 14, 15, 17, 18, 19, 20, 21, 22, 23, 24,
           13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 13, 14, 15, 17, 18, 19, 20, 21, 22, 23, 24, 13, 14, 15, 16,
           17, 18, 19, 20, 21, 22, 23, 24, 13, 14, 15, 19, 20, 21, 22, 23, 24, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22,
           23, 24, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 16,
           17, 18, 19, 21, 22, 23, 24, 16, 17, 18, 19, 20, 21, 22, 23, 24, 16, 17, 18, 19, 20, 21, 22, 23, 24]

    for s, t in zip(src, tar):
        s = s - 1
        t = t - 1
        node_features = ["0"] * nnode
        node_features[s] = "1"
        node_features[t] = "1"
        link = "{}_{}".format(s, t)
        label = "0"
        print(
            "{},{},{},{},{},{},{}".format("|".join(adjs), str(nnode), "|".join(node_features), len(edge_features),
                                          "|".join(["_".join(nf) for nf in edge_features]), link,
                                          label))


def testcase():
    adj, datm = olsr_graph_offline(8)
    datm2 = olsr_link_2hops_offline(8)
    MPR = olsr_node_isMPR_offline(8)

    datmcombined = [[0] * nbnode for _ in range(nbnode)]

    for i in range(nbnode):
        for j in range(nbnode):
            if datm[i][j] == 0 and datm2[i][j] == 0:
                datmcombined[i][j] = 0
            elif datm2[i][j] == 0:
                datmcombined[i][j] = datm[i][j]
            else:
                datmcombined[i][j] = datm2[i][j]

    printmatlab(datm)
    printmatlab(datm2)
    primt(MPR)


import networkx as nx
import numpy as np

import os
import subprocess
import time

if __name__ == '__main__':

    nodeid = 1
    nbnode = 24
    if len(sys.argv) > 1:
        nodeid = int(sys.argv[1])

    # testcase()

    # combine adj with latest information from adj2hop

    dathisto = []

    while True:
        start = time.time()
        olsrinfo = {}
        for i in range(nbnode):
            cm, datm = olsr_graph_online(i)
            datm2 = olsr_link_2hops_online(i)
            isimpr = olsr_node_isMPR_online(i)

            olsrinfo[i] = (datm, dat2m, isimpr)  # keep it short, to stay realtime

            # print("cm")
            # printmatlab(cm)
            # print("datm")
            # printmatlab(datm)
            # printdataset(datm)

        # create dataset

        datmcombined = []
        for i in range(nodeid):
            datm = olsrinfo[i][0]
            datm2 = olsrinfo[i][1]
            datmc = [[0] * nbnode for _ in range(nbnode)]

            for i in range(nbnode):
                for j in range(nbnode):
                    if datm[i][j] == 0 and datm2[i][j] == 0:
                        datmc[i][j] = 0
                    elif datm2[i][j] == 0:
                        datmc[i][j] = datm[i][j]
                    else:
                        datmc[i][j] = datm2[i][j]  # this is last updated information
            datmcombined.append(datmc)

        dathisto.append(datmcombined)
        if len(dathisto) > 5:
            dathisto.pop(0)

        if len(dathisto) == 5:
            printdataset(dathisto)

        # move this block to the bottom of the while loop
        endtime = time.time()
        waittime = endtime - start
        if waittime <= 0:
            continue

        time.sleep(waittime)
        continue

        # for suboptimal schedule

        # graph = csr_matrix(cm)
        # dist_matrix, predecessors = shortest_path(graph, return_predecessors=True, directed=False)
        # print("dist")
        # printmatlab(dist_matrix.tolist())
        # print("pre")
        # printmatlab(predecessors.tolist())

        ncm = np.array(cm)
        G = nx.from_numpy_array(ncm)
        # G = nx.Graph(cm)
        allpaths = nx.shortest_path(G, source=nodeid - 1)
        allpaths = dict(sorted(allpaths.items()))
        print(allpaths)

        schedule = path_to_schedule(allpaths)
        print("schedule", schedule)
        # schedule reducing and ordering

        ready_frame = []
        for slot in schedule:
            ready_frame.append([slot])
        ready_schedule = []
        ready_schedule.append(ready_frame)
        print(ready_schedule)

        # def write_scheduler_reuse(node, schedule, file):
        schedule_name = 'scheduler_olsr.xml'
        write_scheduler_reuse(nbnode, ready_schedule, schedule_name)

        command = "emaneevent-tdmaschedule -i emane0 {0}".format(schedule_name)
        FNULL = open(os.devnull, 'w')
        subprocess.call([command], shell=True, stdout=FNULL, stderr=subprocess.STDOUT)

        time.sleep(5)


def test():
    a = [
        [
            [0, 2105088, 2105088, 0, 0, 0, 2105088, 0, 2105088, 0, 0, 0, 4914944, 0, 0, 2105088, 2105088, 2105088,
             2105088, 2105088, 2105088, 2105088, 2105088, 2105088],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 3153664, 0, 0, 0, 2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2105088, 0, 2105088, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [
                2105088, 3292928, 0, 2105088, 0, 2105088, 0, 2105088, 0, 2105088, 2105088, 0, 0, 0, 0, 0, 2105088, 0,
                3849984, 0, 0, 2735872, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [
                5160704, 2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2105088, 2105088, 0, 0, 0, 2105088, 2105088, 2105088,
                3563264, 2105088, 5504768],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [
                2105088, 0, 0, 0, 2105088, 0, 0, 4275968, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2105088, 2105088, 2105088, 0,
                2105088, 2105088],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [
                2105088, 2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2105088, 0, 0, 0, 0, 0, 0, 2105088, 2105088, 2105088,
                2105088, 2105088, 2105088],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7667456, 0, 2105088, 2105088, 2105088, 0, 0, 0, 0, 0,
             2105088],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        ],
        [
            [0, 2105088, 2105088, 0, 0, 0, 2105088, 0, 2105088, 0, 0, 0, 5570304, 0, 0, 2105088, 2105088, 2105088,
             2105088,
             2105088, 2105088, 2105088, 2105088, 2105088],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 3153664, 0, 0, 0, 2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2105088, 0, 2105088, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [
                2105088, 3292928, 0, 2105088, 0, 2105088, 0, 2105088, 0, 2105088, 2105088, 0, 0, 0, 0, 0, 2105088, 0,
                3849984, 0, 0, 2735872, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [
                5160704, 2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2105088, 2105088, 0, 0, 0, 2105088, 2105088, 2105088,
                3563264, 2105088, 5291776],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [
                2105088, 2105088, 0, 0, 2105088, 0, 0, 3415808, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2105088, 2105088, 2105088,
                0, 2105088, 2105088],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [
                2105088, 2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2105088, 0, 0, 0, 0, 0, 0, 2105088, 2105088, 2105088,
                2105088, 2105088, 2105088],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6864640, 0, 2105088, 2105088, 2105088, 0, 0, 0, 0, 0,
             2105088],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        ],
        [
            [0, 2105088, 2105088, 0, 0, 0, 2105088, 0, 2105088, 0, 0, 0, 5783296, 0, 0, 2105088, 2105088, 2105088,
             2105088,
             2105088, 2105088, 2105088, 2105088, 2105088],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 3153664, 0, 0, 0, 2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2105088, 0, 2105088, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [
                2105088, 3292928, 0, 2105088, 0, 2105088, 0, 2105088, 0, 2105088, 2105088, 0, 0, 0, 0, 0, 2105088, 0,
                3849984, 0, 0, 2735872, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [
                4914944, 2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2105088, 2105088, 0, 0, 0, 2105088, 2105088, 2105088,
                3342080, 2105088, 5291776],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [
                2105088, 2105088, 0, 12975872, 2105088, 0, 0, 3030784, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2105088, 2105088,
                2105088, 0, 2105088, 2105088],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [
                2105088, 2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2105088, 0, 0, 0, 0, 0, 0, 2105088, 2105088, 2105088,
                2105088, 2105088, 2105088],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6864640, 0, 2105088, 2105088, 2105088, 0, 0, 0, 0, 0,
             2105088],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        ],
        [
            [0, 2105088, 2105088, 0, 0, 0, 2105088, 0, 2105088, 0, 0, 0, 0, 0, 0, 2105088, 2105088, 2105088, 2105088,
             2105088, 2105088, 2105088, 2105088, 2105088],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 2940672, 0, 0, 0, 2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2105088],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2105088, 0, 2105088, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [
                2105088, 3292928, 0, 2105088, 0, 2105088, 0, 2105088, 0, 2105088, 2105088, 0, 0, 0, 0, 0, 2105088, 0,
                4112128, 0, 0, 2735872, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [
                5160704, 2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2105088, 2105088, 0, 0, 0, 2105088, 2105088, 2105088,
                3342080, 2105088, 5291776],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [
                2105088, 2105088, 0, 12975872, 2105088, 0, 0, 3030784, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2105088, 2105088,
                2105088, 0, 2105088, 2105088],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [
                2105088, 2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2105088, 0, 0, 0, 0, 0, 0, 2105088, 2105088, 2105088,
                2105088, 2105088, 2105088],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6586112, 0, 2105088, 2105088, 2105088, 0, 0, 0, 0, 0, 0],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        ],
        [
            [0, 2105088, 2105088, 0, 0, 0, 2105088, 0, 2105088, 0, 0, 0, 5979904, 0, 0, 2105088, 2105088, 2105088,
             2105088,
             2105088, 2105088, 2105088, 2105088, 2105088],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 2940672, 0, 0, 0, 2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2105088],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2105088, 0, 2105088, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [
                2105088, 3292928, 0, 2105088, 0, 2105088, 0, 2105088, 0, 2105088, 2105088, 0, 0, 0, 0, 0, 2105088, 0,
                4112128, 0, 0, 2735872, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [
                4882176, 2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2105088, 2105088, 0, 0, 0, 2105088, 2105088, 2105088,
                3129088, 2105088, 5291776],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [
                2105088, 2105088, 0, 11304704, 2105088, 0, 0, 3030784, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2105088, 2105088,
                2105088, 0, 2105088, 2105088],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [
                2105088, 2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2105088, 0, 0, 0, 0, 0, 0, 2105088, 2105088, 2105088,
                2105088, 2105088, 2105088],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6078208, 0, 2105088, 2105088, 2105088, 0, 0, 0, 0, 0, 0],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [2105088, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        ]
    ]

    printdataset(a)
