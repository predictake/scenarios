import json
import datetime


class Transmission:
    def __init__(self):
        self.msg_departure_node = None
        self.msg_departure_date = None
        self.msg_arrival_node = None
        self.msg_arrival_date = None
        self.msg_size = None
        self.msg_type = None
        self.msg_id = None

        self.ack_departure_date = None
        self.ack_arrival_date = None
        self.ack_size = None

        #self.ip_cr = None
        #self.ip_rtt = None


keys = ["MSG Departure Node", "MSG Departure Date", "MSG Arrival Node", "MSG Arrival Date", "MSG Size", "MSG Type",
        "MSG ID",
        "ACK Departure Date", "ACK Arrival Date", "ACK Size",
        "IP Message Mean Round-Trip-Time", "User Message Mean Round-Trip-Time",
        "IP Message Completion Rate", "User Message Completion Rate",
        "IP Messages Sent - NO ACK Received", "IP Messages Received", "IP Messages Sent - ACK Received",
        "User Messages Sent - NO ACK Received", "User Messages Sent - ACK Received",
        "IP Messages Received Mean Processing Time", "User Messages Recipients", "User Messages Received"]


def load_take_stats(filename):
    values = {}
    min_time = None

    stats = {}

    with open(filename) as json_file:
        take_stats_json = json.load(json_file)

        for data, msg in take_stats_json.items():
            # Extract transmission data
            if keys[10] in data:
                stats['ip_rtt'] = float(msg)
            if keys[12] in data:
                stats['ip_cr'] = float(msg)

            if 'Transmission Data' in data:
                n = int(data.split(' ')[2])
                t = Transmission()

                if keys[0] in msg.keys():
                    t.msg_departure_node = int(msg[keys[0]].split('-')[1])
                if keys[1] in msg.keys():
                    t.msg_departure_date = datetime.datetime.strptime(msg[keys[1]], '%Y/%m/%d %H:%M:%S.%f')
                    if min_time is None:
                        min_time = t.msg_departure_date
                    elif min_time > t.msg_departure_date:
                        min_time = t.msg_departure_date
                if keys[2] in msg.keys():
                    t.msg_arrival_node = int(msg[keys[2]].split('-')[1])
                if keys[3] in msg.keys():
                    t.msg_arrival_date = datetime.datetime.strptime(msg[keys[3]], '%Y/%m/%d %H:%M:%S.%f')
                if keys[4] in msg.keys():
                    t.msg_size = msg[keys[4]]
                if keys[5] in msg.keys():
                    t.msg_type = msg[keys[5]]
                if keys[6] in msg.keys():
                    t.msg_id = msg[keys[6]]
                if keys[7] in msg.keys():
                    t.ack_departure_date = datetime.datetime.strptime(msg[keys[7]], '%Y/%m/%d %H:%M:%S.%f')
                if keys[8] in msg.keys():
                    t.ack_arrival_date = datetime.datetime.strptime(msg[keys[8]], '%Y/%m/%d %H:%M:%S.%f')
                if keys[9] in msg.keys():
                    t.ack_size = msg[keys[9]]

                values[n] = t


    return values, min_time, stats
