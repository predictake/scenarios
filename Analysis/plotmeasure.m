
close all
%% laptops

if 0 %load csv file
    clear all
    
    %folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodesolsrmgenA2075msg\20230825-1315\"
    %folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodesolsrmgenA2075msgTCP\20230825-1345\"
        %folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodesolsrsinrtmgenA2075msg\20230825-1415\"
        folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodesolsrsinrtmgenA2075msgTCP\20230825-1445\"
    %folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodesolsrsinrttakeA2A1msg\20230822-0900_r\" %missing routes log
    %folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodesolsrsinrttakeA2A1msg\20230823-1530\" %used for emulation
    
    %folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodesolsrsinrttakeA2A75msg\20230822-0930_m\" %missing take log
    %folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodesolsrsinrttakeA2A75msg\20230822-1530_m\" %missing take log
        %folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodesolsrsinrttakeA2A75msg\20230822-1900\"
    folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodesolsrsinrttakeA2A75msg\20230823-1630\"
    %folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodesolsrsinrttakeA201msg\20230822-1000\"
        %folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodesolsrsinrttakeA2075msg\20230822-1030\"
    %folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodesolsrtakeA2A1msg\20230823-1500\"
    %folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodesolsrtakeA2A1msg\20230824-1500\"
    %folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodesolsrtakeA2A75msg\20230822-1600_m\"
    %folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodesolsrtakeA2A75msg\20230822-1800_l\"
        %folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodesolsrtakeA2A75msg\20230823-1600\"
    %folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodetakeA2A1msg\20230824-1115\"
        %folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodetakeA2A75msg\20230824-1145\"
    %folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\emulations\olsr4msgA2A\"
    %folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\emulations\sinrt4msgA2A\results_data\results_data\"
    
    %%folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodesolsrsinrttakeA2A75msg\20230823-1630\"
    
    %folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\tests\A20TCPmaxbitrateOLSR\"
    %folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\tests\A20TCPmaxbitrateOLSR3\"
    
    
    col = jet(24);
    
    route = 1;
    mgen = 0;

    steps = 1:1000;
    pathlossthreshold = 113;
    nbnode = 6;

    sdLookUp;  % run the sdlookup.m script provided by JFW
    
    lookuptablepair = zeros(nbnode);
    lookuptablepair = sd; 
    
    %for i =1:nbnode
    %    for j = 1:nbnode
    %        lookuptablepair(i,j) = i *10 + j;
    %    end
    %end

    %load take for CR
    if ~mgen
        takefile2 = folder_res + "take\result2.csv";
        opts = delimitedTextImportOptions("NumVariables", 9);
        opts.DataLines = [2, Inf];
        opts.Delimiter = ",";
        opts.VariableNames = ["ts", "t", "src", "dest", "rtt", "cr", "mes", "psize_B", "asize_B"];
        opts.VariableTypes = ["double", "double", "double","double", "double", "double","double", "double", "double"];
        opts.ExtraColumnsRule = "ignore";
        opts.EmptyLineRule = "read";
        traffic = readtable(takefile2, opts);
        
        %compute rate each second
        
    else
        mgenfile2 = folder_res + "mgenlog.csv";
        opts = delimitedTextImportOptions("NumVariables", 9);
        opts.DataLines = [2, Inf];
        opts.Delimiter = ",";
        opts.VariableNames = ["ts", "src", "dest", "win", "rate", "loss", "delaymean", "delaymin", "delaymax", "count"];
        opts.VariableTypes = ["double", "double", "double","double", "double", "double","double", "double", "double", "double"];
        opts.ExtraColumnsRule = "ignore";
        opts.EmptyLineRule = "read";
        traffic = readtable(mgenfile2, opts);
        
        traffic.cr = 1-traffic.loss;
        traffic.rtt = traffic.delaymean;
        
    end
    
    nhfile = folder_res + "route.csv";

    opts = delimitedTextImportOptions("NumVariables", 9);
    opts.DataLines = [2, Inf];
    opts.Delimiter = ",";
    opts.VariableNames = ["ts", "nodei", "dest", "gw"];
    opts.VariableTypes = ["double", "double", "double","double"];
    opts.ExtraColumnsRule = "ignore";
    opts.EmptyLineRule = "read";
    if route
        nh = readtable(nhfile, opts);
    else       
        nh = array2table(zeros(0,4));
        nh.Properties.VariableNames = {'ts','nodei', 'dest','gw'}
    end
    
    hopcountfile = folder_res + "hop.csv";

    opts = delimitedTextImportOptions("NumVariables", 9);
    opts.DataLines = [2, Inf];
    opts.Delimiter = ",";
    opts.VariableNames = ["ts", "nodei", "dest", "hop"];
    opts.VariableTypes = ["double", "double", "double","double"];
    opts.ExtraColumnsRule = "ignore";
    opts.EmptyLineRule = "read";
    if route
        hopc = readtable(hopcountfile, opts);
    else       
        hopc = array2table(zeros(0,4));
        hopc.Properties.VariableNames = {'ts','nodei', 'dest','hop'}
    end
    
    %load RSSI values
    rssifilename = folder_res + "rssi.csv";

    opts = delimitedTextImportOptions("NumVariables", 12);
    opts.DataLines = [2, Inf];
    opts.Delimiter = ",";
    opts.VariableNames = ["mode", "time", "us", "ts", "rate", "rssi1", "rssi2", "da", "sa", "ra", "ta", "bssid", "nodei"];
    opts.VariableTypes = ["double", "datetime", "double","double", "double", "double", "double", "double", "double", "double", "string", "categorical", "double"];
    opts.ExtraColumnsRule = "ignore";
    opts.EmptyLineRule = "read";
    opts = setvaropts(opts, "ta", "WhitespaceRule", "preserve");
    opts = setvaropts(opts, ["ta", "bssid"], "EmptyFieldRule", "auto");
    opts = setvaropts(opts, "time", "InputFormat", "HH:mm:ss.SSSSSS");
    opts = setvaropts(opts, ["us", "rssi1", "rssi2"], "TrimNonNumeric", true);
    opts = setvaropts(opts, ["us", "rssi1", "rssi2"], "ThousandsSeparator", ",");
    rssi = readtable(rssifilename, opts);

    %load routing logs for dat
    nvgfile = folder_res +  "nvg.csv"

    opts = delimitedTextImportOptions("NumVariables", 11);
    % Specify range and delimiter
    opts.DataLines = [2, Inf];
    opts.Delimiter = ",";
    % Specify column names and types
    opts.VariableNames = ["it", "time", "node", "i", "j", "w"]; %time in ms, then IP address
    opts.VariableTypes = ["double", "double", "double", "double","double", "double"];
    % Specify file level properties
    opts.ExtraColumnsRule = "ignore";
    opts.EmptyLineRule = "read";
    % Import the data
    if route
        nvg = readtable(nvgfile, opts);
    else
        nvg = array2table(zeros(0,6));
        nvg.Properties.VariableNames = {'it','time','node','i','j','w'}
    end
    
    routingfile = folder_res +  "routing.csv"

    opts = delimitedTextImportOptions("NumVariables", 11);
    % Specify range and delimiter
    opts.DataLines = [2, Inf];
    opts.Delimiter = ",";
    % Specify column names and types
    opts.VariableNames = ["ts", "node", "srcip", "dstip", "macsrc"]; %time in ms, then IP address
    opts.VariableTypes = ["double", "double", "double", "double","double"];
    % Specify file level properties
    opts.ExtraColumnsRule = "ignore";
    opts.EmptyLineRule = "read";
    % Import the data
    if route
        routing = readtable(routingfile, opts);
    else
        routing = array2table(zeros(0,5));
        routing.Properties.VariableNames = {'ts','node','srcip','dstip','macsrc'}
    end
    
end
    
%% plots
mints = min(traffic.ts);
traffic.t = traffic.ts - mints;

nh.t = nh.ts - mints;
rssi.t = rssi.ts - mints;
nvg.t = nvg.time - mints;
hopc.t = hopc.ts- mints;
routing.t = routing.ts -mints;

rssi = rssi(rssi.t>=0 & rssi.t<1000,:);
nh = nh(nh.t>=0 & nh.t<1000,:);
nvg = nvg(nvg.t>=0 & nvg.t<1000,:);
hopc = hopc(hopc.t>=0 & hopc.t<1000,:);
routing = routing(routing.t>=0 & routing.t<1000,:);

if 0
    
%mean CR
    %figure(1)
    %hold on;
    
    for s = 1:nbnode
        for d = 1:nbnode
            if s==d
                continue;
            end
     
            crs = [];
            rtts = [];
            rates = [];
            for ts = steps
                cr = mean(traffic.cr(traffic.src == s & traffic.dest == d & traffic.t > ts & traffic.t <= ts+1));
                rtt = mean(traffic.rtt(traffic.src == s & traffic.dest == d & traffic.t > ts & traffic.t <= ts+1));
                
                if mgen
                    rate = mean(traffic.rate(traffic.src == s & traffic.dest == d & traffic.t > ts & traffic.t <= ts+1));
                else
                    psizetraffic = traffic(traffic.src == s & traffic.dest == d & traffic.cr == 1 & traffic.t > ts & traffic.t <= ts+1,:);
                    rate = (sum(psizetraffic.psize_B) + sum(psizetraffic.asize_B))*8/1000;
                end
                
                crs = [crs cr];
                rtts = [rtts rtt];
                rates = [rates rate];
                %trafficsd = traffic(traffic.src == s & traffic.dest == d,:);
            end
            figure(1)
            hold on
            scatter3(steps,zeros(length(steps),1)+lookuptablepair(s,d),crs);
            text(steps(end)-50,lookuptablepair(s,d),max(crs),strcat(num2str(s),num2str(d)));
            figure(2)
            hold on
            scatter3(steps,zeros(length(steps),1)+lookuptablepair(s,d),rtts);
            text(steps(end)-50,lookuptablepair(s,d),max(rtts),strcat(num2str(s),num2str(d)));
            figure(3)
            hold on
            scatter3(steps,zeros(length(steps),1)+lookuptablepair(s,d),rates);
            text(steps(end)-50,lookuptablepair(s,d),max(rates),strcat(num2str(s),num2str(d)));
            
        end
        %move mean?
    end
    
    figure(1)
    title(strcat("CR per pair sd at ts=",num2str(mints)))
    xlabel("time [s]")
    ylabel("pairs sd [#]")
    zlabel("Completion Ratio (CR) [%]")
    
    xlim([0 1000])
    
    savefig(strcat(folder_res,"CRpairsd.fig"))
    figure(2)
    title(strcat("RTT per pair sd at ts=", num2str(mints)))
    xlabel("time [s]")
    ylabel("pairs sd [#]")
    zlabel("Round Trip Time (RTT)/Latency [s]")
    
    xlim([0 1000])
    
    savefig(strcat(folder_res,"RTTpairsd.fig"))
    figure(3)
    title(strcat("Rate per pair sd at ts=", num2str(mints)))
    xlabel("time [s]")
    ylabel("pairs sd [#]")
    zlabel("Rate [kbps]")
    
    xlim([0 1000])
    
    savefig(strcat(folder_res,"Ratepairsd.fig"))

    %steps = 1:round(max(res.time))+1;
    meancr = [];
    for ts = steps
       crs = traffic.cr(traffic.t > ts & traffic.t <= ts+1);
       acr = mean(crs)*100;
       %if acr < 100
       %    acr = acr - random(gm);
       %end
       meancr = [meancr acr];
    end
    
    figure()
    hold on

    scatter(steps, meancr); %cr each second

    mvcr = movmean(meancr,10);
    mvcr850 = mvcr(1:850)
    plot(steps,mvcr,'k'); %moving average
    plot([1 steps(end)], [mean(rmmissing(meancr)) mean(rmmissing(meancr))], 'LineWidth', 2, 'Color', 'r') %mean over all scenario

    ylim([0 100])

    title("Completion Ratio sum(rx_{ack})/sum(tx_{msg}) each 1s","and moving average")
    xlabel("time [s]")
    ylabel("Completion Ratio (CR) [%]")
    
    xlim([0 1000])

    legend(["CR 1s-average", "moving average-10s"])
    savefig(strcat(folder_res,"meanCR.fig"))
    
    meanrtt = [];

    for ts = steps
       rtts = traffic.rtt(traffic.t > ts & traffic.t <= ts+1 & traffic.cr == 1);
       meanrtt = [meanrtt mean(rtts)]; %/ 1000
    end
    
    
    figure()
    hold on

    scatter(steps, meanrtt); %cr each second

    mvrtt = movmean(meanrtt,10);
    plot(steps,mvrtt/1000) %,'k'); %moving average
    plot([1 steps(end)], [mean(rmmissing(meanrtt)) mean(rmmissing(meanrtt))], 'LineWidth', 2, 'Color', 'r') %mean over all scenario

    ylim([0 max(meanrtt)])
    ylim([0 30])
    
    xlim([0 1000])


    title("Round Trip Time each 1s","and moving average")
    xlabel("time [s]")
    ylabel("Round Trip Time (RTT) [s] ")

    legend(["RTT 1s", "moving average"])
    savefig(strcat(folder_res,"meanRTT.fig"))
    
    
    meanrates = [];

    for ts = steps
        if mgen
            rate = mean(traffic.rate(traffic.t > ts & traffic.t <= ts+1));
        else
            psizetraffic = traffic(traffic.cr == 1 & traffic.t > ts & traffic.t <= ts+1,:);
            rate = (sum(psizetraffic.psize_B) + sum(psizetraffic.asize_B))*8/1000;
        end

       meanrates = [meanrates rate];
    end
        
    figure()
    hold on

    scatter(steps, meanrates); %cr each second

    mvrate = movmean(meanrates,10);
    plot(steps,mvrate,'k'); %moving average
    plot([1 steps(end)], [mean(rmmissing(meanrates)) mean(rmmissing(meanrates))], 'LineWidth', 2, 'Color', 'r') %mean over all scenario

    %ylim([0 max(meanrates)])
    %ylim([0 30])

    title("Rate each 1s","and moving average")
    xlabel("time [s]")
    ylabel("Rate [kbps] ")

    legend(["Rate 1s", "moving average"])
    savefig(strcat(folder_res,"meanRate.fig"))

   
    figure()
    hold on
    scatter(1/(traffic.rtt/1000), traffic.cr*100)
    
    title("Correlation RTT and CR")
    xlabel("1/RTT [1/s]")
    ylabel("CR [%]")
    savefig(strcat(folder_res,"RTTvsCR.fig"))
    
    
%plot no ack and ack
    figure
    hold on

    %trafficack = traffic(traffic.cr==1,:);
    %trafficnoack = traffic(traffic.cr==0,:);

    %scatter(trafficack.t, trafficack.src*10 + trafficack.dest, 'k')
    %scatter(trafficnoack.t, trafficnoack.src*10 + trafficnoack.dest, 'r')
    for s = 1:nbnode
        for d = 1:nbnode
            if s == d
                continue
            end
            trafficack = traffic(traffic.cr==1 & traffic.src == s & traffic.dest == d,:);
            trafficnoack = traffic(traffic.cr<1 & traffic.src == s & traffic.dest == d,:);
            
            scatter(trafficack.t, zeros(1,length(trafficack.t))+lookuptablepair(s,d), 'k')
            scatter(trafficnoack.t,zeros(1,length(trafficnoack.t))+lookuptablepair(s,d), 'r')
            tt = max([max(trafficnoack.t) max(trafficack.t)]);
            
            if length(tt) > 0
                text(tt+50,lookuptablepair(s,d),strcat(num2str(s),num2str(d)));
            end
        end
    end
    
    title("ACK (black) no ACK (red)")
    xlabel("time [s]")
    ylabel("Pair (CR) [%]")
    
    xlim([0 1000])
    
    savefig(strcat(folder_res,"acknoack.fig"))

end


if 0
   
%plot next hop
    figure
    hold on

    %diffnhdest = nh(nh.dest ~= nh.gw,:);

    %%scatter3(nh.ts, nh.nodei*10 + nh.dest, nh.gw);
    %scatter3(diffnhdest.t, diffnhdest.nodei*10 + diffnhdest.dest, diffnhdest.gw);
    
    for s = 1:nbnode
        for d = 1:nbnode
            diffnhdest = nh(nh.dest ~= nh.gw & nh.nodei == s & nh.dest == d,:);
            scatter3(diffnhdest.t, zeros(1,length(diffnhdest.t))+lookuptablepair(s,d), diffnhdest.gw, 15, diffnhdest.gw);
            if length(diffnhdest.t) > 0
                text(950,lookuptablepair(s,d),max(diffnhdest.gw),strcat(num2str(s),num2str(d)));
            end
        end
    end
    
    h = colorbar;
    h.Label.String = "Next hop [#]";
    
    %plot with colorcode = nexthop
    title("Next Hop")
    xlabel("time [s]")
    ylabel("Pair sd [#]")
    zlabel("Next hop [#]")
    
    xlim([0 1000])
    
    savefig(strcat(folder_res,"nexthop.fig"))
    
%hop count
    figure
    hold on
    %scatter3(hopc.t, hopc.nodei*10 + hopc.dest, hopc.hop);
    
    for s = 1:nbnode
        for d = 1:nbnode
            htmp = hopc(hopc.nodei==s & hopc.dest ==d,:);
            scatter3(htmp.t, zeros(1,length(htmp.t))+lookuptablepair(s,d), htmp.hop, 15, htmp.hop);
            if length(htmp.t) > 0
                text(950,lookuptablepair(s,d),max(htmp.hop),strcat(num2str(s),num2str(d)));
            end
        end
    end
    
    h = colorbar;
    h.Label.String = "Hop count [#]";

    
    title("Hop count")
    xlabel("time [s]")
    ylabel("Pair sd [#]")
    zlabel("Hop count [#]")

    xlim([0 1000])
    
    savefig(strcat(folder_res,"hopcount.fig"))
    

%plot nvg
    figure 
    hold on

    %scatter3(nvg.t,nvg.i*10+nvg.j, nvg.w, 10, nvg.w)
    
    for i = 1:nbnode
        for j = 1:nbnode
            ntmp = nvg(nvg.i==i & nvg.j == j & nvg.t < 900,:);
            scatter3(ntmp.t, ntmp.node*50+lookuptablepair(i,j), ntmp.w, 15, ntmp.w);

        end
    end
    
    for x = 1:nbnode
        for i = 1:nbnode
            for j = 1:nbnode        
                text(950,lookuptablepair(i,j)+x*50,0,strcat(num2str(i),num2str(j)));
            end
        end
    end
    
    
    h = colorbar;
    h.Label.String = "DAT/Link Cost [#]";

    title("Link Cost")
    xlabel("time [s]")
    ylabel("Pair sd [#]")
    zlabel("DAT/Link Cost [#]")

    xlim([0 1000])

    savefig(strcat(folder_res,"linkcost.fig"))
    
    
    figure
    hold on
    
    for s = 1:nbnode
        for d = 1:nbnode
            rtemp = routing(routing.srcip==s & routing.dstip == d,:);
            scatter3(rtemp.t, zeros(1,length(rtemp.t))+lookuptablepair(s,d), rtemp.macsrc, 15, rtemp.macsrc);
            if length(rtemp.t) > 0
                text(950,lookuptablepair(s,d),max(rtemp.macsrc),strcat(num2str(s),num2str(d)));
            end
        end
    end
    
    h = colorbar;
    h.Label.String = "last src MAC [#]";
    
    title("Routing lasthop")
    xlabel("time [s]")
    ylabel("Pair sd [#]")
    zlabel("last hop [#]")

    xlim([0 1000])
    
    savefig(strcat(folder_res,"lasthop.fig"))
    
end


%plot CR vs RSSI
if 1
         
    %take only direct path to plot CR vs RSSI for each pair
    %1 filter the direct path using nexthop
    %2 find the traffic time at which a pair is directly connected and map to the rssi
    %3 select 1s before and 1s after the traffic time (since the laptops have 1-2s clock shift)
    
    figure()
    hold on

    savecrs = []
    saverssis = []
    
    for s = 1:nbnode
        for d = 1:nbnode
            if s == d
                continue
            end

            pairs = [];
            CRs = [];
            RSSIs = [];
            stdRSSIs = [];

            gws_old = nan;
            for t = 1:900
                %t
                gws = nh.gw(nh.nodei == s & nh.dest == d & nh.t > t-1 & nh.t < t); %if Nan take last nh
                if ~isnan(gws)
                    gws_old = gws;
                end

                cr = nan;
                maxrssi = nan;
                pair = nan;
                crs = 0;
                if ~isnan(gws_old)
                    crs = traffic.cr(traffic.src == s & traffic.dest == d & traffic.t > t-1 & traffic.t < t);
                    if length(crs)==0
                        continue
                    end
                    ant = rssi(rssi.sa == s & rssi.nodei == d & rssi.t >t-2 & rssi.t < t+2,:);
                    maxrssi = max(ant.rssi1,ant.rssi2);
                end
                
                %pairs = [pairs s*10+d];
                pairs = [pairs lookuptablepair(s,d)];
                CRs = [CRs mean(crs)];
                RSSIs = [RSSIs mean(maxrssi)];
                stdRSSIs = [stdRSSIs std(maxrssi)];
                
                savecrs = [savecrs mean(crs)];
                saverssis = [saverssis mean(maxrssi)];
                
            end

            scatter3(pairs, RSSIs, CRs)
            if length(RSSIs) > 100
                text(lookuptablepair(s,d),-10,1,strcat(num2str(s),num2str(d)))
            end
        end
    end

    title("RSSI vs CR")
    xlabel("Pair sd [#]")
    ylabel("RSSI [dBm]")
    zlabel("CR [%]")
    
    savefig(strcat(folder_res,"rssivscr.fig"))


    rssifilter = rssi(rssi.nodei == rssi.da,:);

    figure
    scatter(max(rssifilter.rssi1, rssifilter.rssi2),rssifilter.rate)
    
    title("RSSI vs Rate")
    xlabel("RSSI [dBm]")
    ylabel("Rate [Mbps]")
    
    savefig(strcat(folder_res,"rssivsrate.fig"))

end


if 0 %plot RSSI over time (fading)
    
    %figure
    %scatter3(rssi.t, rssi.sa*10 + rssi.nodei, max(rssi.rssi1,rssi.rssi2))

    figure
    hold on

    rmean = {};
    rnode = {};
    it = 1;

    for s =1:nbnode
        for d= 1:nbnode
            if s == d
                continue
            end
            rssisd = rssi(rssi.sa == s & rssi.nodei == d,:);
            rssimean = movmean(max(rssisd.rssi1,rssisd.rssi2), 100);
            
            %scatter3(rssisd.t, rssisd.sa*10 + rssisd.nodei, max(rssisd.rssi1,rssisd.rssi2));
            %plot3(rssisd.t, rssisd.sa*10 + rssisd.nodei,rssimean)
            
            scatter3(rssisd.t, zeros(1,length(rssisd.t))+lookuptablepair(s,d), max(rssisd.rssi1,rssisd.rssi2));
            plot3(rssisd.t, zeros(1,length(rssisd.t))+lookuptablepair(s,d), rssimean)
            text(950,lookuptablepair(s,d),-10,strcat(num2str(s),num2str(d)))

            rmean{it} = max(rssisd.rssi1,rssisd.rssi2) - rssimean;
            rnode{it} = strcat(num2str(s),num2str(d));
            
            it = it +1;
        end
    end
    
    title("RSSI")
    xlabel("time [s]")
    ylabel("Pair sd [#]")
    zlabel("RSSI [dBm]")
    
    xlim([0 1000])
    
    savefig(strcat(folder_res,"rssitime.fig"))
    
    for i =1:length(rmean)
        figure 
        hold on
        
        rm  = rmean{i};
        n = rnode{i};
        histogram(rm, 'BinWidth',1)
        %bar(rm)

        xlim([-10 10])
        ylim([0 10e4])
        
        title(strcat("Fading on link", n))
        xlabel("RSSI-mean(RSSI) [dB]")
        ylabel("Count [#]")
        
        savefig(strcat(folder_res, "fadingonlink",n,".fig"))
    end
    
end