import os
import zipfile
import subprocess

import sys

sys.path.append("../")

from pairs import extract_stats
from queuemerge import queue_extractor
from routes_to_csv import traversals
from olsr3_log import extract_statsolsr3
from olsr.olsr_graph_extraction import olsr_graph_offline_log

from monitor_rssi import extract_rssi
from mgenlog import mgen_extract

folder = "C:/Users/Yann/switchdrive/Armasuisse/MAInet118730/publications/WiMob2023/tests/threshold/nofading/" \
         "SINR42_pl113_RSSI-66/2/results_data/results_data/"

folder = "C:/Users/Yann/switchdrive/Armasuisse/MAInet118730/DashBoard/olsr3/results_data/results_data/"

folder = "C:/Users/Yann/switchdrive/Armasuisse/MAInet118730/theorytoODR/take_olsr2s/"
folder = "X:/result_data_olsr3/results_data/results_data/"

folder = "X:/olsr/take_olsrbinary/results_data/results_data/"

folder = "X:/olsr/take_olsrsinrtadaptative/results_data/results_data/"

# olsr
folder = "X:/olsr/take_olsrclassical/fading/.5traffic/results_data/results_data/"
# folder = "X:/olsr/take_olsrclassical/fading/results_data/results_data/"
folder = "X:/olsr/take_olsrclassical/fading/2traffic/results_data/results_data/"

# olsr+sinrt
# folder = "X:/olsr/take_olsr3sinrt/fading/.5traffic/results_data/results_data/"
# folder = "X:/olsr/take_olsr3sinrt/fading/results_data/results_data/"
# folder = "X:/olsr/take_olsr3sinrt/fading/2traffic/results_data/results_data/"

folder = "X:/olsr/take_olsr3sinrt/fading/2trafficobliviousschedule/results_data/results_data/"
folder = "X:/olsr/take_olsr3sinrt/fading/2trafficobliviousschedule/2slots/results_data/results_data/"

folder = "X:/olsr/take_fm/results_data/results_data/"

# schedule and olsrsinrt
folder = "X:/olsr/take_olsr3sinrt/fading/2traffictlvtraffic/results_data/results_data/"
folder = "X:/olsr/take_olsr3sinrtoblivious/nofading/2traffic/results_data/results_data/"
folder = "X:/olsr/take_olsr3sinrttlvtraffic/nofading/2traffic/results_data_degree/results_data/"

folder = "X:/olsr/take_olsrsinrtadaptative/fading/results_data/results_data/"
folder = "X:/olsr/take_olsr3sinrtoblivious/fading/delay_tc_interval/results_data/results_data/"

# folder = "X:/olsr/take_olsr3sinrt/nofading_34dBthreshold/2traffic/results_data/results_data/"


folder = "X:/olsr/take_olsr3sinrt/fading/realisitc_1msg/results_data/results_data/"
folder = "X:/olsr/take_olsr3sinrt/fading/2traffictlvtraffic/results_data/results_data/"

folder = "X:/olsr/anglovaB/"
take_folder = "take_fading/"


# first find all results_data folder

def get_results_folder(root_folder, folder_res):
    try:
        content = os.listdir(root_folder)
        for c in content:
            if "results_data" == c:
                print("add results dir", root_folder)
                folder_res[0].append(root_folder + c + "/")
                return folder_res
            elif "results_data.zip" in c:
                print("add missing zip alone", root_folder)
                folder_res[1].append(root_folder)

            folder_res = get_results_folder(root_folder + c + "/", folder_res)

    except WindowsError:
        pass
        # print('No Result dir')

    return folder_res


folder_res = [[], []]
folder_res = get_results_folder(folder, folder_res)


# unzip if needed
def unzip_results(folder_res):
    for fr in folder_res:
        print("extracting", fr)
        with zipfile.ZipFile(fr + "results_data.zip", 'r') as zip_ref:
            zip_ref.extractall(fr + "results_data/")


print("to unzip:", folder_res[1])
unzip_results(folder_res[1])


# test if generated

def is_generated(folder_res):
    folder_gen = []
    for fr in folder_res:
        try:
            content = os.listdir(fr + take_folder)
        except WindowsError:
            fr += "results_data/"
            try:
                content = os.listdir(fr + take_folder)
            except WindowsError:
                print("missing results_data folder", fr)
                continue

        if "result.csv" not in content:  # or
            folder_gen.append(fr)

    return folder_gen


folder_res = is_generated(folder_res[0])
print("to process:", folder_res)

folder_res = ["X:/olsr/anglovaB/A2A_1msg/flowcontrol/results_data2/results_data/",
              "X:/olsr/anglovaB/A2A_1msg/flowcontrol/results_data3/results_data/",
              "X:/olsr/anglovaB/A2A_1msg/flowcontrol/results_data4/results_data/",
              "X:/olsr/anglovaB/A2A_1msg/flowcontrol/results_data5/results_data/"]  # manually added

folder_res = ["X:/olsr/anglovaB/A2A_2msg/exotic/results_data_alpha1/results_data/",
              "X:/olsr/anglovaB/A2A_2msg/exotic/results_data_alpha0.1/results_data/",
              "X:/olsr/anglovaB/A2A_2msg/exotic/results_data_alpha4/results_data/"]

folder_res = [
    "X:/Results/laptop/experiments/results_all_laptops/bk_measure/LaptopShow-6nodesolsrmgenA2075msg/20230825-1315/",

    "X:/Results/laptop/experiments/results_all_laptops/bk_measure/LaptopShow-6nodesolsrmgenA2075msgTCP/20230825-1345/",

    "X:/Results/laptop/experiments/results_all_laptops/bk_measure/LaptopShow-6nodesolsrsinrtmgenA2075msg/20230825-1415/",

    "X:/Results/laptop/experiments/results_all_laptops/bk_measure/LaptopShow-6nodesolsrsinrtmgenA2075msgTCP/20230825-1445/",

    "X:/Results/laptop/experiments/results_all_laptops/bk_measure/LaptopShow-6nodesolsrsinrttakeA2A1msg/20230822-0900/", #missing routes
    "X:/Results/laptop/experiments/results_all_laptops/bk_measure/LaptopShow-6nodesolsrsinrttakeA2A1msg/20230823-1530/",

    "X:/Results/laptop/experiments/results_all_laptops/bk_measure/LaptopShow-6nodesolsrsinrttakeA2A75msg/20230822-0930_m/",
    "X:/Results/laptop/experiments/results_all_laptops/bk_measure/LaptopShow-6nodesolsrsinrttakeA2A75msg/20230822-1530_m/",
    "X:/Results/laptop/experiments/results_all_laptops/bk_measure/LaptopShow-6nodesolsrsinrttakeA2A75msg/20230822-1900/",
    "X:/Results/laptop/experiments/results_all_laptops/bk_measure/LaptopShow-6nodesolsrsinrttakeA2A75msg/20230823-1630/",

    "X:/Results/laptop/experiments/results_all_laptops/bk_measure/LaptopShow-6nodesolsrsinrttakeA201msg/20230822-1000/",

    "X:/Results/laptop/experiments/results_all_laptops/bk_measure/LaptopShow-6nodesolsrsinrttakeA2075msg/20230822-1030/",

    "X:/Results/laptop/experiments/results_all_laptops/bk_measure/LaptopShow-6nodesolsrtakeA2A1msg/20230823-1500/",
    "X:/Results/laptop/experiments/results_all_laptops/bk_measure/LaptopShow-6nodesolsrtakeA2A1msg/20230824-1500/",

    "X:/Results/laptop/experiments/results_all_laptops/bk_measure/LaptopShow-6nodesolsrtakeA2A75msg/20230822-1600_m/",
    "X:/Results/laptop/experiments/results_all_laptops/bk_measure/LaptopShow-6nodesolsrtakeA2A75msg/20230822-1800_m/",
    "X:/Results/laptop/experiments/results_all_laptops/bk_measure/LaptopShow-6nodesolsrtakeA2A75msg/20230823-1600/",

    "X:/Results/laptop/experiments/results_all_laptops/bk_measure/LaptopShow-6nodetakeA2A1msg/20230824-1115/",
    "X:/Results/laptop/experiments/results_all_laptops/bk_measure/LaptopShow-6nodetakeA2A75msg/20230824-1145/",

    "X:/Results/laptop/experiments/results_all_laptops/bk_measure/emulations/olsr4msgA2A/",
    "X:/Results/laptop/experiments/results_all_laptops/bk_measure/emulations/sinrt4msgA2A/results_data/results_data/",

    "X:/Results/laptop/experiments/results_all_laptops/bk_measure/tests/A20TCPmaxbitrateOLSR/",
    "X:/Results/laptop/experiments/results_all_laptops/bk_measure/tests/A20TCPmaxbitrateOLSR3/"

]


folder_res = [folder_res[-1]]

macs = {"60:e3:27:10:b8:c6": 1, "60:e3:27:0a:8c:38": 2, "60:e3:27:10:8a:cf": 3, "c4:e9:84:1d:a2:bc": 4,
        "60:e3:27:12:a2:a7": 5, "60:e3:27:10:c8:86": 6}

laptop = True

for fr in folder_res:
    print("processing:", fr)
    folder = fr  # + "results_data/"


    if laptop:
        routingfile = open(folder + "routing.csv", "w")
        routingfile.write("time,nodei,srcip,dstip,macsrc\n")

        filter = '(tcp or udp) and !packetbb' #for tcp replace by TCP and UDP?
        fields = "-e frame.time_epoch -e ip.src -e ip.dst -e eth.src -e eth.dst"
        for i in range(1, 6 + 1):

            cmd = "-E header=y -E separator=, -E quote=d -E occurrence=f "+ fields +" > "+folder + f"log/{i}/output_{i}.csv".format(i)

            cmds = ['C:/Program Files/Wireshark/tshark.exe']
            cmds.append("-r")
            cmds.append(folder + f"log/{i}/interface.pcap")
            cmds.append("-Y")
            cmds.append(filter)
            cmds.append("-T")
            cmds.append("fields")

            cmds += cmd.split(" ")

            print(cmds)
            process = subprocess.Popen(cmds,shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            process.wait()

            try:
                with open(folder + f'\log\{i}\output_{i}.csv',"r") as f:
                    lines = f.readlines()
                    header = True
                    for line in lines:
                        if header:
                            header = False
                            continue

                        frame = line.split(",")
                        t = frame[0]
                        try:
                            srcip = int(frame[1].replace('"','').split(".")[-1])
                            dstip = int(frame[2].replace('"','').split(".")[-1])

                            macsrc = macs[frame[3].replace('"','')]
                            macdst = macs[frame[4].strip().replace('"','')]
                        except Exception as e:
                            print(e)
                            continue

                        if i == dstip and dstip == macdst and macsrc != srcip:
                            routingfile.write("{},{},{},{},{}\n".format(t, i, srcip, dstip, macsrc))
            except:
                print("missing interface log", i)
                pass
        routingfile.flush()
        routingfile.close()


    min_time = None
    min_time = extract_stats(folder + "take/", '{}/transmission_stats.json')
    import datetime

    # min_time = datetime.datetime(2023, 8, 22, 10, 30, 36, 322000)

    if not laptop:
        min_time2 = extract_stats(folder + "take_fading/", '{}/transmission_stats.json')

        if min_time2 > min_time:
            min_time = min_time2  # take the latest min time.
    print("make sure to set the correct min_time")
    """
    try:
        queue_extractor(folder + "queues/", "queue{0}.csv", min_time)
    except:
        print("[W] missing queue info in", folder)
    try:
        traversals(folder + "routes/", 'routing_{}.csv', min_time)
    except:
        print("[W] missing route info in", folder)
    """
    try:
        routefile = open(folder + "route.csv", "w")
        routefile.write("time,nodei,dest,gw,cost\n")

        csvfile = open(folder + "nvg.csv", "w")
        csvfile.write("time,nodei,s_dat,t_dat,dat\n")

        hopfile = open(folder + "hop.csv", "w")
        hopfile.write("time,nodei,dest,gw,hop\n")

        for i in range(1, 6 + 1):
            olsr_graph_offline_log(i, folder + "log/", csvfile, routefile, hopfile)
    except:
        print("[W] missing OLSR info in", folder)

    try:
        extract_statsolsr3(folder, min_time)
    except:
        print("[W] missing OLSR3 info in", folder)

    extract_rssi(folder)

    try:
        mgen_extract(folder)
    except:
        print("missing MGEN, exit")
