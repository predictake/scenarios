import os

folder_name = ""
min_time = 0


# SINR
# 1679576187.2259216,0,2:-66.1999969482,3:-72.9000015259,4:-81.1999969482,5:-83.3999938965,6:-86.8000030518,7:-75.3000030518,9:-75.3000030518,10:-86.8999938965,11:-88.6000061035,23:-69.0999984741,24:-72.0
# NEIG
# 1679576333.4600978,20,10.100.0.16,10.100.0.17,10.100.0.18

def extract_SINR(t, line, node, csvf, folder):
    for elem in line:
        sinr = elem.split(":")

        csvf.write("{},{},{},{},{}\n".format(t, 0, node, sinr[0], sinr[1]))


def extract_neigh(t, line, node, csvf, folder):
    for elem in line:
        neigh = elem.split(".")[-1]
        csvf.write("{},{},{},{}\n".format(t, 20, node, neigh))


# 1680188990.3177638,40,13,17,21,(23, 0.01, 1000000.0, 0)
csvschedule = None


def extract_schedule(t, line, node, csvf, folder):
    global csvschedule
    if csvschedule is None:
        csvschedule = open(folder + "schedule.csv", "w")

    for i, l in enumerate(line):
        csvschedule.write("{},{},{}\n".format(t, node, l, i))


def extract_schedule2(t, line, node, csvf, folder):
    global csvschedule
    if csvschedule is None:
        csvschedule = open(folder + "schedule.csv", "w")

    for i, l in enumerate(line):
        l = l.split(":")[0]
        csvschedule.write("{},{},{}\n".format(t, node, l, i))


# 1689587907.0020323,24,1:2:66,1:3:72,1:16:27,16:2:56,16:9:69,16:1:27,16:3:67,16:7:72,16:17:33,16:18:22,16:19:54,16:21:69,16:22:65,16:23:66,16:24:69,2:1:66,2:13:0,2:16:56,18:1:0,18:3:72,18:2:0,18:9:72,18:16:22,18:17:21,18:19:65,18:20:73,18:22:70,18:23:70,18:24:68,3:1:72,3:4:15,3:5:21,3:6:30,3:7:23,3:8:36,3:9:16,3:10:34,3:11:28,3:12:10,3:16:67,3:18:72,17:2:0,17:9:70,17:16:33,17:1:0,17:18:21,17:19:64,17:20:72,17:21:67,17:22:69,17:23:72,17:24:73,20:2:0,20:13:67,20:16:0,20:1:0,20:17:72,20:18:73,20:19:0,20:21:14,20:23:17,20:24:23,9:3:16,9:4:12,9:5:22,9:6:29,9:7:18,9:8:29,9:10:31,9:11:10,9:12:20,9:16:69,9:17:70,9:18:72,13:14:21,13:15:23,13:20:67
csvnvg = None
csvhop = None
it = 0


import networkx as nx


def shortest_path_to_all_destinations(edges, source, nbnode=6, weight="weight", other=None):
    G = nx.DiGraph()
    G.add_weighted_edges_from(edges)  # wedges

    allroutes = {}
    routes = {}
    missingdest = []

    try:
        allroutes = nx.shortest_path(G, weight=weight)
        if source in allroutes.keys():
            routes = allroutes[source]

    except nx.exception.NodeNotFound as e:
        pass

    hops = {}
    for dest, path in routes.items():
        if dest != source:
            hops[dest] = len(path)-1
    return hops


def extract_nvg(t, line, node, csvf, folder):
    global csvnvg
    global csvhop
    global it
    if csvnvg is None:
        csvnvg = open(folder + "nvg.csv", "w")
        csvhop = open(folder + "hop.csv", "w")

    edges = []
    for l in line:
        a = l.split(":")
        if len(a) != 3:
            print(a)
        s, d, w = a
        csvnvg.write("{},{},{},{},{},{}\n".format(it, t, node, s, d, w))
        edges.append((int(s), int(d), float(w)))

    # compute the number of hops
    hops = shortest_path_to_all_destinations(edges,node, weight=None)
    for d, h in hops.items():
        csvhop.write("{},{},{},{}\n".format(t, node, d, h))

    # it+=1


csvnexthop = None
resampling = [0]*7

def extract_route(t, line, node, csvf, folder):
    global csvnexthop
    global resampling
    if csvnexthop is None:
        csvnexthop = open(folder + "route.csv", "w")

    t = float(t)
    if t - resampling[node] > 5:
        resampling[node] = t
        for elem in line:
            dest, gw = elem.split(":")  # [-1]
            csvf.write("{},{},{},{},{}\n".format(t, 26, node, dest, gw))
            t = float(t)
            csvnexthop.write("{},{},{},{}\n".format(t, node, dest, gw))


logenum = {"RSSI": 0, "MRSSI": 1, "LRSSI": 2, "LEVEL": 3, "THRES": 4, "GPS": 5,  # topology, terrain info
           "QUEU": 10, "TRAFIN": 11, "TRAFOUT": 12, "MQUEU": 13, "MTRAF": 14,  # user traffic
           "NEIG": 20, "MPRL": 21, "MPRR": 22, "2HOP": 23, "NVG": 24, "COST": 25, "ROUT": 26, "DAT": 27,
           "PATH": 28, "MISS": 29, "ROUT1": 200, "MCOST": 201,
           # routing protocol
           "DDFC": 30, "CCFC": 31, "SCFC": 32, "ADFC": 33, "MRES": 34, "MFC": 35,  # flow control
           "OSCHE": 40, "TSCHE": 41, "ASCHE": 42, "HSCHE": 43, "ACTIV": 44, "TRAV": 45
           # "QSCHE": 42  # scheduling
           }

logenum = {"SINR": (0, extract_SINR), "GPS": (1,),  # topology, terrain info
           "QUEU": (10,), "TRAFIN": (11,), "TRAFOUT": (12,),  # user traffic
           "NEIG": (20, extract_neigh), "MPRL": (21,), "MPRR": (22,), "2HOP": (23,), "NVG": (24, extract_nvg),
           "COST": (25,),
           "ROUT": (26, extract_route),
           # routing protocol
           "DDFC": (30,),  # flow control
           "OSCHE": (40, extract_schedule), "TSCHE": (41, extract_schedule), "HSCHE": (43, extract_schedule),
           # "ASCHE": (44, extract_schedule2)
           # scheduling
           }


def readstats(root, folder, file, node):
    csvfile = open(folder + "olsr3_{}.csv".format(node), "w")

    with open(folder + file, "r") as lines:
        for line in lines.readlines():
            params = line.strip().split(",")
            t = params[0]
            m = int(params[1])
            if len(params) > 2 and len(params[2]) > 0:
                for ext in logenum.values():
                    if len(ext) > 1:
                        if m == ext[0]:
                            ext[1](t, params[2:], node, csvfile, root)


def extract_statsolsr3(folder_arg, mtime):
    global min_time
    global folder_name

    folder_name = folder_arg + "logs/features/"
    min_time = mtime

    files = {}
    # content = None

    step = 1

    try:
        content = os.listdir(folder_name)
    except WindowsError:
        print('No Result dir')
        return -1

    for c in content:
        if os.path.isfile(folder_name + c) and "olsr3features_" in c:
            n = int(c.split(".")[0].split("_")[1])
            files[n] = (folder_name, c)

    # load
    for n, f in files.items():
        readstats(folder_arg, f[0], f[1], n)


def test(folder):
    with open(folder + "/logs/log/schedule.csv", "w") as schedfile:
        for i in ranage(1, 24 + 1):
            with open(folder + "/logs/log/features/olsr3features_{}".format(i), "r") as logfile:
                for line in logfile.readlines():
                    params = line.split(",")
                    if len(params) > 2:
                        print(param[1])


if __name__ == '__main__':
    folder = "C:/Users/Yann/switchdrive/Armasuisse/MAInet118730/publications/WiMob2023/tests/threshold/fading/SINR42_pl113_RSSI-66/"
    extract_stats(folder)
