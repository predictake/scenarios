# folders_res = ["X:/Results/laptop/experiments/results_all_laptops/bk_measure/LaptopShow-6nodesolsrmgenA2075msg/20230825-1315/",
#               "X:/Results/laptop/experiments/results_all_laptops/bk_measure/LaptopShow-6nodesolsrmgenA2075msgTCP/20230825-1345/"]

# folders_res = ["X:/Results/laptop/experiments/results_all_laptops/bk_measure/LaptopShow-6nodesolsrmgenA2075msgTCP/20230825-1345/"]


file = "mgen/1/mgen.drc"  # A20

# "07:54:42.095345 REPORT proto>UDP flow>61 src>10.100.0.6/6006 dst>10.100.0.1/5001 window>1.013529 rate>269.947875 kbps loss>0.000000 latency ave>0.081763 min>0.074114 max>0.119213, count>76"

import datetime


def mgen_extract(folder_res):
    csvmgen = open(folder_res + "mgenlog.csv", "w")
    csvmgen.write("ts,src,dest,w,rate,loss,avgmean,avgmin,avgmax,cout\n")

    #datestring = "20230825"
    #datestring = folder_res.split("/")[7].split("-")[0]
    #year = int(datestring[0:4])
    #month = int(datestring[4:6])
    #day = int(datestring[6:])

    with open(folder_res + file, "r") as f:
        lines = f.readlines()
        for line in lines:
            if "REPORT" in line:
                data = line.split(" ")
                time = data[0]  # need to change the time

                ts = datetime.datetime.strptime(time, '%H:%M:%S.%f')
                ts = ts.replace(year=2023, month=8, day=25, hour=ts.hour + 2).timestamp()

                src = data[4].split(">")[1].split("/")[0][-1]
                dest = data[5].split(">")[1].split("/")[0][-1]
                w = data[6].split(">")[1]
                rate = data[7].split(">")[1]
                loss = data[9].split(">")[1]
                avgmean = data[11].split(">")[1]
                avgmin = data[12].split(">")[1]
                avgmax = data[13].split(">")[1].split(",")[0]
                cout = data[14].split(">")[1]

                csvmgen.write(f"{ts},{src},{dest},{w},{rate},{loss},{avgmean},{avgmin},{avgmax},{cout}")

    csvmgen.flush()
    csvmgen.close()


if __name__ == '__main__':
    mgen_extract("X:/Results/laptop/experiments/results_all_laptops/bk_measure/tests/A20TCPmaxbitrateOLSR3/")