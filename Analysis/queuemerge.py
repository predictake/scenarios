import datetime

def queue_extractor(folder, filename, min_time, nbnode=24):

    firsttime = None
    queues = {}
    timeorder = {}
    version = 2

    for i in range(1, nbnode + 1):
        queues[i] = []
        print(i)
        with open(folder + filename.format(i), "r") as file:
            lines = file.readlines()
            for line in lines:
                if version == 1:
                    splitline = line.strip().split(",")

                    if len(splitline) > 1:  # valid line
                        q = {}
                        t = int(splitline[0])

                        # if firsttime is None:
                        #    firsttime = t
                        # elif firsttime > t:
                        #    firsttime = t

                        for que in splitline[1:]:
                            dest = que.split(":")
                            if len(dest) == 2:
                                if t not in timeorder.keys():
                                    timeorder[t] = []
                                timeorder[t].append((i, dest[0], dest[1]))  # (s,t,l)

                elif version == 2:
                    # line = "1656932557151,6 ; 14:0,8 ; 14:0,13 ; 10:0,14 ; 8:0,24 ; 4:0,9:0,12:0,16:0,19:0,20:0,21:0,22:0,23:0,"
                    splitline = line.strip().split(";")
                    if len(splitline) > 1:
                        q = {n: 0 for n in range(1, nbnode + 1)}
                        datefsrcip = splitline[0].split(",")
                        t = int(datefsrcip[0])

                        validt = datetime.datetime.fromtimestamp(t/1000.)
                        if validt <= min_time:
                            continue

                        fsrcip = datefsrcip[1]

                        for srcdstip in splitline[1:]:
                            for d in srcdstip.split(","):
                                deby = d.split(":")
                                if len(deby) > 1:
                                    # if deby[0] not in q.keys():
                                    #    q[deby[0]] = 0
                                    try:
                                        ss = int(deby[0])
                                        if ss <= nbnode:
                                            q[ss] += int(deby[1])
                                    except:
                                        pass
                                        # print(line)
                                        # print(deby)
                                else:
                                    fsrcip = deby  # last can fail

                        if t not in timeorder.keys():
                            timeorder[t] = []
                        for k, b in q.items():
                            timeorder[t].append((i, k, b))  # (s,t,l)

    with open(folder + "mergedqueue.csv", "w") as file:
        file.write("time,src,dest,len\n")
        timeorder = sorted(timeorder.items())
        mintime = timeorder[0][0]
        for queues in timeorder:
            # if len(queues[1])> 2:
            #    print("re")
            for queue in queues[1]:
                file.write("{},{},{},{}\n".format(queues[0], queue[0], queue[1], queue[2]))


if __name__ == '__main__':
    filename = "queue{0}.csv"
    folder = "C:/Users/Yann/Desktop/queues/odr/realistic/os/"
    folder = "C:/Users/Yann/Desktop/testalllogs/queues/"
    folder = "C:/Users/Yann/switchdrive/Armasuisse/MAInet118730/publications/WiMob2023/tests/nofading/olsr/queues/"
    folder = "C:/Users/Yann/switchdrive/Armasuisse/MAInet118730/PV/PV_2023-03-13/queues/"

    nbnode = 24

    queue_extractor(folder, filname, nbnode)
