#!/usr/bin/python

__author__ = "Yann Maret"
__copyright__ = "Copyright 2019, HEIA-FR - ISTake Project"
__date__ = "2020"

import os, sys

sys.path.append('../')

from conf.config import *

# from usertrafficfile import read_usertraffic_file

ROUTES_FILENAME = '{}_routes.csv'
ROUTES_FILENAME = 'routing{}.csv'

TRAVERSAL_FILENAME = "traversals.csv"
FREQUENCY_FILENAME = "frequency.csv"
NEXTHOP_FILENAME = "nexthop.csv"
OLSR_METRIC_FILENAME = "olsr_metric.csv"

ROUTES_CSV_FILEPATH = MATLAB_FOLDER


# import matlab.engine

# eng = matlab.engine.connect_matlab()


def routes_stats(file, src, routes):
    header = True
    with open(file, 'r') as f:
        for line in f.readlines():
            if header:
                header = False
                continue
            param = line.strip().split(',')
            if int(param[0]) not in routes.keys():
                routes[int(param[0])] = {}
            if int(src) not in routes[int(param[0])].keys():
                routes[int(param[0])][int(src)] = {}
            #       time,          src,      dest,             gw,           hops,         metric
            routes[int(param[0])][int(src)][int(param[1])] = [int(param[2]), int(param[3]), int(param[4])]

    return routes


class GroupbyTime:
    def __init__(self, groupebytime=1):
        self.groupebytime = groupebytime
        self.groupeby = []

    def append(self, t, id, data):
        for groupe in self.groupeby:
            pass


def routes_stats2(file, src, routes):
    header = True
    with open(file, 'r') as f:
        for line in f.readlines():
            if header:
                header = False
                continue
            param = line.strip().split(',')
            if int(float(param[0])) not in routes.keys():
                routes[int(float(param[0]))] = {}
            if int(param[1]) not in routes[int(float(param[0]))].keys():
                routes[int(float(param[0]))][int(param[1])] = {}
            ##       time,          src,      dest,             gw,           hops,         metric
            routes[int(float(param[0]))][int(param[1])][int(param[2])] = [int(param[3]), 0,
                                                                     0]  # int(param[3]), int(param[4])]

            # if int(src) not in routes.keys():
            #    routes[int(src)] = []

            # routes[int(param[1])].append((float(param[0]), int(param[2]), int(param[3])))

    return routes


def gather_routes(wd, file):
    """
    gather_routes(wd,file)
    :param wd:
    :param file:
    :return:
    """

    # gather node number
    content = os.listdir(wd)
    routes_node = {}
    for c in content:
        # f = wd + c + '/' + file.format(c)
        # if c.isdigit() and os.path.isfile(f):
        f = wd + '/' + c
        a = c.split(".")[0].split("_")[-1]
        if a.isdigit() and os.path.isfile(f):
            routes_node[int(a)] = f

    # gather files
    routes = {}
    for node, file in routes_node.items():
        # routes = routes_stats(route_d, file.format(route_f), route_f, routes)
        # print route_d, route_f
        routes = routes_stats2(file, node, routes)
        # print routes

    return routes_node, routes


def path(route, src, dest):
    """
    path return the path from src to dest with intermediate nodes

    :param routes:
    :param src:
    :param dest:
    :return:
    """
    if src == dest or src not in route.keys() or dest not in route[src].keys():
        #print('INPUT')
        return []
    p = []
    s = src
    while route[s][dest][0] != dest:
        s = route[s][dest][0]
        if s not in p:
            p.append(s)
        else:
            #print('loop?', t, src, dest, 'info', s, p)
            break
        if s not in route.keys():
            #print('source', t, src, dest, 'info', s, p)
            break
        elif dest not in route[s].keys():
            #print('break', t, src, dest, 'info', s, p)
            break

    return p


def routes(routes, take, output, time):
    """
    routes()
    :WARNING, synch between take and routes -> take+1 = routes

    :param wd:
    :param file:
    :param pairs: if provided export the routes according the take obj
    :return:
    """
    # sort by time routes and take
    take_sorted = sorted(take.keys())
    routes_sorted = sorted(routes.keys())
    min_take = take_sorted[0]
    min_route = routes_sorted[0]

    time_consumed = False

    csv = open(output, 'w')

    for t_take in take_sorted:
        #print('*********', t_take)
        traffic = take[t_take]
        t_traffic = t_take - min_take

        # TODO add time filter if needed
        # if t_traffic >

        for mes in traffic:
            for t_route in routes_sorted:
                #print('*********', t_route)
                route = routes[t_route]
                t_route = t_route - min_route

                if t_route >= t_traffic:
                    # WARNING: add +1 to the take nodes to be compared with the nodes exporter
                    p = path(route, mes.source + 1, mes.recipient[0] + 1)
                    csv.write('{},{},{},{}\n'.format(t_traffic, mes.source + 1, mes.recipient[0] + 1, p))
                    break


def traversals2(wd, file, pairs=None, plot=False):
    routes_node, routes = gather_routes(wd, file)

    traversal_f = open(wd + TRAVERSAL_FILENAME, 'w')

    # pick a source node and a destination
    travers = {}
    loopscount = 0
    for src, infos in routes.items():
        for time, dest, gw in infos:

            # for d in range(1, 24 + 1):

            trav = [0] * 24
            loops = []
            loops_detected = False
            while gw != dest:
                # search a time that is slitly before
                lastt = None
                for infos in routes[gw]:
                    t, d, g = infos
                    if d != dest:
                        continue
                    if t > time:
                        break
                    lastt = t
                if lastt is None:
                    break

                if gw in loops:
                    loopscount += 1
                    loops_detected = True
                    break
                loops.append(gw)

                trav[g - 1] += 1
                gw = g

            if loops_detected:
                continue

            # traves[str(time)]

            # groupe each second...
            if int(time) not in travers.keys():
                travers[int(time)] = trav
                print(travers)
            else:
                for i, tr in enumerate(trav):
                    travers[int(time)][i] = tr

    print(travers)

import datetime

def traversals(wd, file, min_time=None, pairs=None, plot=False):
    """
    traversal(wd, file, plot)
    :param wd:
    :param file:
    :param plot:
    :return:
    """

    # gather the routes from the experiments
    routes_node, routes = gather_routes(wd, file)

    """
    gather traversals
    """
    traversal_f = open(wd + TRAVERSAL_FILENAME, 'w')

    t_sorted = sorted(routes.keys())
    min_t = t_sorted[0]
    # print routes
    # print t_sorted
    step = 0
    hist_routes = []
    for t in t_sorted:

        validt = datetime.datetime.fromtimestamp(t)
        if min_time is not None:
            if validt <= min_time:
                continue

        step += 1
        #print('*********', t)
        route = routes[t]
        total_gw = []
        for src in routes_node.keys():  # do it over pairs instead of all to all if available
            for j in routes_node.keys():
                # print src, j
                if src == j or src not in route.keys() or j not in route[src].keys():
                    continue
                gw = []
                s = src
                while route[s][j][0] != j:
                    s = route[s][j][0]
                    if s not in gw:
                        gw.append(s)
                    else:
                        #print('loop?', t, src, j, 'info', s, gw)
                        break
                    if s not in route.keys():
                        #print('source', t, src, j, 'info', s, gw)
                        break
                    elif j not in route[s].keys():
                        #print('break', t, src, j, 'info', s, gw)
                        break

                total_gw += gw
                # if len(gw) > 0:
                #    print 'gw->', gw

        # his = []
        for src in routes_node.keys():
            his = total_gw.count(src)
            # if his == 0:
            #    his = 'NaN'
            traversal_f.write("{},{},{}\n".format(step, src, his))
            hist_routes.append([step, src, his])
            # traversal_f.write("{},{},{}\n".format((t - min_t) / 1000., src, his))

        traversal_f.flush()
        if plot:
            import matplotlib.pyplot as plt
            import seaborn as sns

            # matplotlib histogram
            plt.hist(total_gw, color='blue', edgecolor='black',
                     bins=int(180 / 1))

            print(total_gw)

            # plt.tight_layout()
            plt.show()

    if False:
        c = matlab.double(hist_routes)
        eng.workspace['routes'] = c
        # a = eng.movie_routing_tables(step, 24, c)


def routes_frequency(wd, file, pairs=None, plot=False):
    """
    traversal(wd, file, plot)
    :param wd:
    :param file:
    :param plot:
    :return:
    """

    # gather the routes from the experiments
    routes_node, routes = gather_routes(wd, file)

    """
    gather traversals
    """
    frequency_f = open(wd + FREQUENCY_FILENAME, 'w')

    t_sorted = sorted(routes.keys())
    min_t = t_sorted[0]
    # print routes
    # print t_sorted
    step = 0
    hist_routes = []
    for t in t_sorted:
        step += 1
        print('*********', t)

        route = routes[t]
        total_txs = []

        weights = {}

        srcs = routes_node.keys()
        js = routes_node.keys()
        if pairs is not None:
            srcs, js = pairs

        for src in srcs:  # do it over pairs instead of all to all if available
            for j in js:
                # print src, j
                if src == j or src not in route.keys() or j not in route[src].keys():
                    continue
                txs = []
                s = src
                while route[s][j][0] != j:

                    if s not in weights.keys():
                        weights[s] = {}
                        weights[s][route[s][j][0]] = 1
                    elif route[s][j][0] not in weights[s].keys():
                        weights[s][route[s][j][0]] = 1
                    else:
                        weights[s][route[s][j][0]] += 1

                    s = route[s][j][0]
                    if s not in txs:
                        txs.append(s)
                    else:
                        print('loop?', t, src, j, 'info', s, txs)
                        break
                    if s not in route.keys():
                        print('source', t, src, j, 'info', s, txs)
                        break
                    elif j not in route[s].keys():
                        print('break', t, src, j, 'info', s, txs)
                        break

        for src, gws in weights.items():
            for gw, weight in gws.items():
                frequency_f.write("{},{},{},{}\n".format(step, src, gw, weight))


def olsr_metric_DAT(wd, file):
    """
    olsr_metric_DAT(wd, file)
    :param wd:
    :param file:
    :return:
    """
    # gather the routes from the experiments
    routes_node, routes = gather_routes(wd, file)
    dat = open(wd + OLSR_METRIC_FILENAME, 'w')

    t_sorted = sorted(routes.keys())
    min_t = t_sorted[0]

    for t in t_sorted:
        print('*********', t)
        route = routes[t]
        for src in routes_node.keys():
            for j in routes_node.keys():
                metric = 'nan'
                if src in route.keys() and j in route[src].keys():
                    metric = route[src][j][2]
                dat.write('{},{},{},{}\n'.format((t - min_t) / 1000., src, j, metric))

    dat.close()


def nexthop(wd, file, pairs=None, plot=False):
    # gather the routes from the experiments
    routes_node, routes = gather_routes(wd, file)

    """
    gather nexthop
    """
    nexthop_f = open(wd + NEXTHOP_FILENAME, 'w')

    t_sorted = sorted(routes.keys())
    min_t = t_sorted[0]
    # print routes
    # print t_sorted
    step = 0
    for t in t_sorted:
        step += 1
        print('*********', t)
        route = routes[t]
        for src in routes_node.keys():
            for j in routes_node.keys():
                # print src, j
                if src == j or src not in route.keys() or j not in route[src].keys():
                    continue
                dest = j
                hop = route[src][dest][0]
                nexthop_f.write("{},{},{}\n".format(step, src, dest, hop))


def main(results_routes_folder, user_traffic_file="", output="", param=[], time=0):
    # run over X output (results) folders
    # take_obj_time, take_obj_source = read_usertraffic_file(user_traffic_file)
    # TODO load as well the result route files
    #   routes_node, routes_list = gather_routes(results_routes_folder, ROUTES_FILENAME)

    # routes(routes_list, take_obj_time, output, time)

    # old syntax
    traversals(results_routes_folder, ROUTES_FILENAME)
    # routes_frequency(results_routes_folder, ROUTES_FILENAME)
    # olsr_metric_DAT(wd, file)


if __name__ == '__main__':
    # default parameters
    results_routes_folder = RESULTS_FOLDER + 'optimal_schedule/22062020_1_2_company1_TDMA_MD_custom_pcr/node_exporter/'
    user_traffic_file = USERTRAFFICS_FOLDER + 'scenario_take_inc_100.json'
    output = ROUTES_CSV_FILEPATH + 'default_routes_to_csv.csv'
    time = 0

    """
    import pandas as pd
    import datetime
    import random
    import time
    random.seed(1)

    t = []
    d = []
    for i in range(10):
        time.sleep(random.randint(0,100)/100)
        t.append(datetime.datetime.fromtimestamp(time.time()))
        d.append(1)

    d = {'datetime': t, "data": d}
    df = pd.DataFrame(data=d)

    df.groupby(df["datetime"].dt.second)

    # check input parameters
    if len(sys.argv) > 4 + 1:
        file = sys.argv[1]
        output = sys.argv[2]
        time = sys.argv[3]
    """

    results_routes_folder = 'X:/Users/Nicolas/Results_CP1_OLSRd2_fading_m0s0/20210910-102530_2_3_company1_TDMA_MD_custom_pcr/node_exporter/'
    output = 'traversals.csv'

    results_routes_folder = "C:/Users/Yann/switchdrive/Armasuisse/MAInet118730/publications/WiMob2023/tests/threshold/fading/SINR40_pl115_RSSI-68/routes/"
    results_routes_folder = "C:/Users/Yann/switchdrive/Armasuisse/MAInet118730/publications/WiMob2023/tests/threshold/fading/SINR42_pl113_RSSI-66/nosmallscalefading/2/results_data/results_data/"

    arg = [results_routes_folder, user_traffic_file, output, time]

    # call main
    main(results_routes_folder, user_traffic_file, output, time=time)

    # wd = 'C:/Users/lte/PycharmProjects/Emane/Results/ICMCIS2020/olsr_odr/06022020_1_1_company1_TDMA_MD_custom_pcr/node_exporter_fake_odr/'
    # wd = 'C:/Users/lte/PycharmProjects/Emane/Results/test_traversals/node_exporter/'
    # wd = 'C:/Users/lte/PycharmProjects/Emane/Results/ICMCIS2020/10022020_11_1_company1_TDMA_MD_custom_pcr/node_exporter/'

    # for i in range(1, 11):
    #    wd = 'D:/02032020_1_{}_company1_TDMA_MD_custom_pcr/node_exporter/'.format(i)
    #    file = '{}_routes.csv'
