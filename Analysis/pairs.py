#!/usr/bin/python

__author__ = "Yann Maret"
__copyright__ = "Copyright 2019, HEIA-FR - ISTake Project"
__date__ = "2019"

# rename as take status export to matlab

import sys
import os

sys.path.append('../')

import json
import datetime
import pandas as pd

from library.takelog import load_take_stats
from conf.config import *

MEAN_RTT = []
MEAN_CR = []


def get_batch(root_folder):
    try:
        content = os.listdir(root_folder)
    except WindowsError:
        print('No Result dir')
        return

    return content


def extract_stats(folder_arg, file_arg):
    folder_name = folder_arg
    file_name = file_arg

    folder = []
    # content = None

    step = 1

    try:
        content = os.listdir(folder_name)
    except WindowsError:
        #print('No Result dir')
        return -1

    for c in content:
        if os.path.isdir(folder_name + c) and any(char.isdigit() for char in c):
            folder.append(int(c))

    # load
    stats = {}
    min_time = None
    for i in folder:
        #print(folder_name + file_name.format(i))
        stats[i], mt, _ = load_take_stats(folder_name + file_name.format(i))
        if min_time is None:
            min_time = mt
        elif mt is None:
            continue
        elif min_time > mt:
            min_time = mt

    # print stats
    print(min_time)

    # gather
    state_time = {}
    with open(folder_arg +"result2.csv", "w") as realf:
        realf.write('timestamp,time,src,dest,rtt,cr,nbMessage,psize_B,asize_B\n')
        # f.write('time,src,dest,rtt,cr,nbMessage\n')
        for n, stat in stats.items():
            for td, s in stat.items():
                if s.msg_departure_node != n:  # avoid routing node if any?
                    continue
                # t = (s.ack_arrival_date - min_time).total_seconds()
                t = (s.msg_departure_date - min_time).total_seconds()

                t = str(t)
                if t not in state_time.keys():
                    state_time[t] = {}

                src = s.msg_departure_node
                if src not in state_time[t].keys():
                    state_time[t][src] = {}

                dest = s.msg_arrival_node
                if dest not in state_time[t][src].keys():
                    state_time[t][src][dest] = None

                if s.msg_arrival_node:
                    # delivery ratio
                    pass

                psize = s.msg_size  # s.ack_size
                if s.ack_arrival_date is None or s.msg_departure_date is None:
                    cr = 0.
                    rtt = 0.
                else:
                    cr = 1.
                    rtt = (s.ack_arrival_date - s.msg_departure_date).total_seconds() * 1000
                    psize = s.msg_size + s.ack_size

                #timestampre = datetime.datetime.strptime(str(s.msg_departure_date), '%Y-%m-%d %H:%M:%S.%f').timestamp()
                secsep = str(s.msg_departure_date).split('.')
                timestampre = datetime.datetime.strptime(secsep[0], '%Y-%m-%d %H:%M:%S').timestamp()
                if len(secsep) > 1:
                    timestampre = timestampre + float("0." + secsep[1])

                if state_time[t][src][dest] is None:
                    state_time[t][src][dest] = [rtt, cr, 1, [], [], []]  # [psize], [timestampre]]
                else:
                    state_time[t][src][dest][0] += rtt
                    state_time[t][src][dest][1] += cr
                    state_time[t][src][dest][2] += 1
                    state_time[t][src][dest][3].append(rtt)
                    state_time[t][src][dest][4].append(cr)
                    state_time[t][src][dest][5].append(s.ack_arrival_date)

                    # state_time[t][src][dest][5].append(psize)
                    # state_time[t][src][dest][5].append(timestampre)

                realf.write("{},{},{},{},{},{},{},{},{}\n".format(timestampre, t, src, dest, rtt, cr, 1, s.msg_size,
                                                                  "NaN" if s.ack_size is None else s.ack_size))

                """
                if state_time[t][src][dest][2] > 1:
                    print("t")

                secsep = str(s.msg_departure_date).split('.')
                timestampre = datetime.datetime.strptime(secsep[0], '%Y-%m-%d %H:%M:%S').timestamp()
                #
                if len(secsep) > 1:
                    realf.write('{},{},{}\n'.format(timestampre + float("0." + secsep[1]), src,
                                                    dest))  # stat[1] received it or not, stat[2] number of received packet
                else:
                    realf.write('{},{},{}\n'.format(timestampre, src,
                                                    dest))  # stat[1] received it or not, stat[2] number of received packet
                """
    # compute
    for t, srcs in state_time.items():
        for src, dests in srcs.items():
            for dest, stat in dests.items():
                stat[0] = stat[0] / stat[2]
                stat[1] = stat[1] / stat[2]
                # stat[3] = stat[3] / stat[2]

    # save
    stats_rtt = [0, 0]
    stats_cr = [0, 0]
    with open(folder_arg + 'result.csv', 'w') as f:
        f.write('time,src,dest,rtt,cr,nbMessage\n')
        for t, srcs in state_time.items():
            for src, dests in srcs.items():
                for dest, stat in dests.items():
                    rtt = stat[0]
                    if stat[0] == 0:
                        rtt = 'NaN'
                    else:
                        stats_rtt[0] += rtt
                        stats_rtt[1] += 1.
                    stats_cr[0] += stat[1]
                    stats_cr[1] += 1.
                    # for acktime in stat[5]:
                    #    f.write('{},{},{},{},{},{}\n'.format(acktime, src, dest, rtt, stat[1], stat[
                    #        2]))  # stat[1] received it or not, stat[2] number of received packet

                    f.write('{},{},{},{},{},{}\n'.format(t, src, dest, rtt, stat[1], stat[
                        2]))  # stat[1] received it or not, stat[2] number of received packet

                    # compute for each node rtt and CR
                    # _rtt = stat[0]
                    # _cr = state[1]
                    # _nbofmessages = stat[2]

                    # list_state_per_src[src] = [old_rtt+_rtt,cr,iterator++]

    # plot here the stat
    # for each src
    # list_state_per_src[src][0-1]/[2]

    # stats
    if stats_rtt[1] == 0:
        m_rtt = 0
    else:
        m_rtt = stats_rtt[0] / stats_rtt[1]
    if stats_cr[1] == 0:
        m_cr = 0
    else:
        m_cr = stats_cr[0] / stats_cr[1]

    MEAN_CR.append(m_cr)
    MEAN_RTT.append(m_rtt)

    print(m_cr)
    print(m_rtt)

    return min_time


if __name__ == '__main__':
    folder = None
    file = '{}/transmission_stats.json'

    if len(sys.argv) == 2:
        folder = sys.argv[1]
    elif len(sys.argv) == 3:  # folder
        folder = sys.argv[1]
        file = sys.argv[2]
    else:
        folder = RESULTS_FOLDER

    # or per folder
    folder = RESULTS_FOLDER + '3/'  # + '../02032020_1_10_company1_TDMA_MD_custom_pcr/take/'
    folder = "D:/jfwmatlab/20220201-172907_1_1_company1_TDMA_MD_custom_pcr_interval/take/"
    folder = "D:/jfwmatlab/20220201-175153_2_1_company1_TDMA_MD_custom_pcr_interval/take/"
    folder = "D:/jfwmatlab/20220201-182147_3_1_company1_TDMA_MD_custom_pcr_interval/take/"
    folder = "D:/jfwmatlab/20220201-184420_4_1_company1_TDMA_MD_custom_pcr/take/"
    folder = "D:/jfwmatlab/20220201-190658_5_1_company1_TDMA_MD_custom_pcr/take/"
    folder = "X:/Results/dev/20220303-120131_1_1_company1_TDMA_MD_custom_pcr/take/"
    folder = "X:/Results/dev/20220303-164409_1_1_company1_TDMA_MD_custom_pcr/take/"
    folder = "C:/Users/Yann/Desktop/8n_8/secondos/take/"
    folder = "C:/Users/Yann/Desktop/takeGNN-t300/anglovab/take/"
    folder = "X:/Results/dev/td83/20220530-150116_1_2_company1_TDMA_MD_custom_pcr/take/"
    folder = "C:/Users/Yann/Desktop/synchtake/takewaitdev1auto/take/"
    folder = "C:/Users/Yann/switchdrive/Armasuisse/MAInet118730/DashBoard/dev3/take/1/take/"
    folder = "X:/Results/dev8/analysis/20220819-142141_1_1_company1_TDMA_MD_custom_pcr/take/"
    # extract_stats(folder, file)
    # exit(0)

    folder = "X:/Results/laptop/experiments/yanntest/3laptops_static_1/test1"
    folder = "X:/Results/laptop/experiments/results_all_laptops/Laptop-6nodesolsrtakeRealistic4/20230117-0918"
    folder = "X:/olsr/take_olsr3/test2"
    folder = "C:/Users/Yann/Desktop/olsr/routes/anglova/olsr3"
    folder = "C:/Users/Yann/Desktop/olsr/routes/anglova/anglova"

    folder = "C:/Users/Yann/Desktop/testalllogs"
    folder = "C:/Users/Yann/switchdrive/Armasuisse/MAInet118730/publications/WiMob2023/tests/threshold/fading/SINR40_pl115_RSSI-68"

    extract_stats(folder + "/take/", file)

    exit(0)

    folder = "X:/Results/dev8/analysis/"
    folder = "X:/Results/laptop/experiments/yanntest/3laptops_static_2/"

    for sample in get_batch(folder):
        if os.path.isdir(folder + sample + "/take/"):
            extract_stats(folder + sample + "/take/", file)

    exit(0)

    subfolder = ["classicallastrx", "classicallastrxfading", "classicalodr", "classicalodrfading",
                 "classicaltxslotrate",
                 "classicaltxslotratefading", "disconnectionlastrx", "disconnectionlastrxfading", "disconnectionodr",
                 "disconnectionodrfading", "disconnectiontxslotrate", "disconnectiontxslotratefading"]

    subfolder = ["repeatability"]

    # root_folder = "X:/Results/dev/td83/new/"
    root_folder = "X:/Results/dev8/"
    for sub in subfolder:
        for sample in get_batch(root_folder + sub + "/"):
            if os.path.isdir(root_folder + sub + "/" + sample + "/take/"):
                extract_stats(root_folder + sub + "/" + sample + "/take/", file)
            else:
                MEAN_CR.append([])
                MEAN_RTT.append([])
            print(MEAN_CR)
            print(MEAN_RTT)

    exit(0)
    extract_stats(folder, file)

    # content = os.listdir(folder)
    # result_folder = []
    # for c in content:
    #   if os.path.isdir(folder + c) and '$' not in c:
    #       result_folder.append(folder+c+'/take/')

    # for res in result_folder:
    #    extract_stats(res, file)

    print(MEAN_CR)
    print(MEAN_RTT)
