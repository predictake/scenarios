clear all;
close all;

folder_th = "C:\Users\Yann\Desktop\traffic_analysis";

mobilityfile = "C:\Users\Yann\Desktop\traffic_analysis\company_1.csv";
trafficfile = "C:\Users\Yann\Desktop\traffic_analysis\taketraffic_anglova_default.csv";
%trafficfile = 

folder_res = "C:\Users\Yann\switchdrive\Armasuisse\MAInet118730\publications\WiMob2023\tests\threshold\fading\SINR42_pl113_RSSI-66\nosmallscalefading\3\results_data\results_data\";
folder_res = "C:\Users\Yann\switchdrive\Armasuisse\MAInet118730\DashBoard\olsr3\results_data\results_data\"
folder_res = "C:\Users\Yann\switchdrive\Armasuisse\MAInet118730\theorytoODR\take_olsr2s"

folder_res = "x:\result_data_olsr3\results_data\results_data"
folder_res = "x:\olsr\take_olsrlogsinr\results_data\results_data"
folder_res = "x:\olsr\take_olsr1sinr\results_data_alpha1\results_data"
%folder_res = "x:\olsr\take_olsrbinary\results_data\results_data"

folder_res = "x:\olsr\take_olsrsinrtadaptative\results_data\results_data"


%olsr
%folder_res = "x:\olsr\take_olsrclassical\fading\.5traffic\results_data\results_data"
%folder_res = "x:\olsr\take_olsrclassical\fading\results_data\results_data"
folder_res = "x:\olsr\take_olsrclassical\fading\2traffic\results_data\results_data"

%olsr sinrt
%folder_res = "x:\olsr\take_olsr3sinrt\fading\.5traffic\results_data\results_data"
%folder_res = "x:\olsr\take_olsr3sinrt\fading\results_data3\results_data"
folder_res = "x:\olsr\take_olsr3sinrt\fading\2traffic\results_data\results_data"

%folder_res = "x:\olsr\take_olsr3sinrt\fading\2trafficobliviousschedule\results_data\results_data"
folder_res = "x:\olsr\take_olsr3sinrt\fading\2trafficobliviousschedule\2slots\results_data\results_data"

%scheduling
%folder_res = "X:\olsr\take_olsr3sinrtoblivious\nofading\2traffic\results_data\results_data"
%folder_res = "x:\olsr\take_olsr3sinrt\nofading_34dBthreshold\2traffic\results_data\results_data"
%folder_res = "X:\olsr\take_olsr3sinrttlvtraffic\nofading\2traffic\results_data_degree\results_data"

%folder_res = "X:\olsr\take_olsrsinrtadaptative\fading\results_data\results_data"
folder_res = "X:\olsr\take_olsr3sinrtoblivious\fading\delay_tc_interval\results_data\results_data"


folder_res = "X:\olsr\take_olsr3sinrttlvtraffic\fading\2traffic\results_data\results_data"


folder_res = "X:\olsr\take_olsr3sinrt\fading\realisitc_1msg\results_data\results_data"
folder_res = "X:\olsr\take_olsr3sinrt\fading\realisitc_1msg\oblivious\results_data\results_data"

folder_res = "X:\olsr\take_olsr3sinrt\fading\2traffic\allMPRs\results_data\results_data"


folder_res = "X:\olsr\take_olsr3sinrt\fading\realistic_2msg\request\results_data\results_data"
folder_res = "X:\olsr\take_olsr3sinrt\fading\realistic_2msg\tlv\results_data\results_data"
folder_res = "X:\olsr\take_olsr3sinrt\fading\realistic_2msg\oblivious\results_data\results_data"
folder_res = "X:\olsr\take_olsr3sinrt\fading\2traffictlvtraffic\results_data\results_data"

folder_res = "X:\olsr\take_olsr3sinrt\fading\realistic_2msg\results_data\results_data"

%folder_res = "x:\olsr\take_olsr3sinrt\fading\2traffic\results_data\results_data"

%% A02
folder_res = "X:\olsr\anglovaB\A20\rr\results_data\results_data"
%folder_res = "X:\olsr\anglovaB\A20\oblivious\results_data\results_data"
%folder_res = "X:\olsr\anglovaB\A20\tlv\results_data\results_data"
folder_res = "X:\olsr\anglovaB\A20\slotrequest\results_data\results_data"


%folder_res = "X:\olsr\anglovaB\realistic_2msg\request\results_data\results_data"

folder_res = "X:\olsr\anglovaB\realistic_3msg\rr\results_data\results_data"

folder_res = "X:\olsr\anglovaB\A2A_1msg\rr\results_data\results_data"
folder_res = "X:\olsr\anglovaB\A2A_1msg\rr\stable\results_data\results_data"
folder_res = "X:\olsr\anglovaB\A2A_1msg\rr\balanced\results_data\results_data"

folder_res = "X:\olsr\anglovaB\A2A_3msg\odr113_os\results_data_os2s\results_data\results_data"
folder_res = "X:\olsr\anglovaB\A2A_3msg\odr113_os\results_data_rr_no_m\results_data"

%folder_res = "X:\olsr\anglovaB\A2A_2msg\odr113\results_data_2sscheduleupdate\results_data\results_data"
%folder_res = "X:\olsr\anglovaB\A2A_2msg\odr113\results_data_rr_no_m\results_data\results_data"
%folder_res = "X:\olsr\anglovaB\A2A_1msg\odr113_os\results_data_os2s\results_data\results_data"
%folder_res = "X:\olsr\anglovaB\A2A_1msg\odr113_os\results_data1msg_nomanagement_odr\results_data\results_data"
%folder_res = "X:\olsr\anglovaB\A2A_1msg\odr113_os\results_data_rr\results_data\results_data"

folder_res = "X:\olsr\anglovaB\A2A_2msg\rr\results_data\results_data"
folder_res = "X:\olsr\anglovaB\A2A_2msg\rr\allMPRs\results_data\results_data"
%folder_res = "X:\olsr\anglovaB\A2A_2msg\rr\stable\results_data\results_data"


folder_res = "X:\olsr\anglovaB\A2A_1msg\flowcontrol\results_data5\results_data"
folder_res = "X:\olsr\anglovaB\A2A_1msg\rr\results_data\results_data"

folder_res = "X:\olsr\anglovaB\realistic_2msg\flowcontrol+oblivious\results_data\results_data"


folder_res = "X:\olsr\anglovaB\A2A_1msg\exotic\olsr3_1_sinr\results_data_alpha0.1\results_data"
folder_res = "X:\olsr\anglovaB\A2A_1msg\exotic\olsr3_1_sinr\results_data_alpha1\results_data"
folder_res = "X:\olsr\anglovaB\A2A_1msg\olsr\results_data\results_data"

folder_res = "X:\olsr\anglovaB\A2A_1msg\adaptivethreshold\results_data.4\results_data"
folder_res = "X:\olsr\anglovaB\A2A_2msg\exotic\results_data_alpha1\results_data"

folder_res = "X:\olsr\anglovaB\A2A_2msg\exotic\results_data_alpha1\results_data"

%% laptops olsr/SINRT

% A20
folder_res="X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodesolsrsinrttakeA201msg\20230822-1000"
folder_res="X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodesolsrsinrttakeA2075msg\20230822-1030"

% A2A
folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodesolsrsinrttakeA2A1msg\20230822-0900"
folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodesolsrsinrttakeA2A1msg\20230823-1530"

%folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodesolsrsinrttakeA2A75msg\20230822-0930"
%folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodesolsrsinrttakeA2A75msg\20230822-1530"
folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodesolsrsinrttakeA2A75msg\20230822-1900"
folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodesolsrsinrttakeA2A75msg\20230823-1630"

folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodesolsrtakeA2A1msg\20230823-1500"

%folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodesolsrtakeA2A75msg\20230822-1600"
folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodesolsrtakeA2A75msg\20230822-1800"
folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodesolsrtakeA2A75msg\20230823-1600"

%measure no route
folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodetakeA2A1msg\20230824-1115"

folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodetakeA2A75msg\20230824-1145"

%emulation OLSR/SINRT
folder_res ="X:\Results\laptop\experiments\results_all_laptops\bk_measure\emulations\sinrt4msgA2A"

folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\emulations\olsr4msgA2A"


%to plot
folder_res = "X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodesolsrsinrttakeA2A1msg\20230823-1530"




%no fading
queuefile = folder_res + "\queues\mergedqueue.csv";
schedulefile = folder_res + "\logs\features\schedule.csv"
%schedulefile = folder_res + "\logs\log\features\schedule.csv"
routefile = folder_res + "\routes\traversals.csv";
%takefile = folder_res + "\take_fading\results.csv";
%takefile2 = folder_res + "\take_fading\result2.csv";

takefile = folder_res + "\take\results.csv";
takefile2 = folder_res + "\take\result2.csv";

steps = 1:1000;
pathlossthreshold = 113;
nbnode = 24;

col = jet(24);

%% load company (eel)
opts = delimitedTextImportOptions("NumVariables", 11);
% Specify range and delimiter
opts.DataLines = [2, Inf];
opts.Delimiter = ",";
% Specify column names and types
opts.VariableNames = ["t", "dist", "i_nem_id", "i_lat", "i_lon", "i_alt", "j_nem_id", "j_lat", "j_lon", "j_alt", "pathloss"]; %time in ms, then IP address
opts.VariableTypes = ["double", "double", "double", "double","double", "double", "double", "double","double", "double", "double"];
% Specify file level properties
opts.ExtraColumnsRule = "ignore";
opts.EmptyLineRule = "read";
% Import the data
mobility = readtable(mobilityfile, opts);

%% load imposed traffic (take or mgen)
opts = delimitedTextImportOptions("NumVariables", 3);
opts.DataLines = [2, Inf];
opts.Delimiter = ",";
opts.VariableNames = ["t", "src", "dest"];
opts.VariableTypes = ["double", "double", "double"];
opts.ExtraColumnsRule = "ignore";
opts.EmptyLineRule = "read";
thtraffic = readtable(trafficfile, opts);

%% load measured traffic
opts = delimitedTextImportOptions("NumVariables", 9);
opts.DataLines = [2, Inf];
opts.Delimiter = ",";
opts.VariableNames = ["ts", "t", "src", "dest", "rtt", "cr", "mes", "psize_B", "asize_B"];
opts.VariableTypes = ["double", "double", "double","double", "double", "double","double", "double", "double"];
opts.ExtraColumnsRule = "ignore";
opts.EmptyLineRule = "read";
traffic = readtable(takefile2, opts);

%% load queue log
opts = delimitedTextImportOptions("NumVariables", 4);
opts.DataLines = [2, Inf];
opts.Delimiter = ",";
opts.VariableNames = ["ts", "src", "dest", "len"]; %time in ms, then IP address
opts.VariableTypes = ["double", "double", "double", "double"];
opts.ExtraColumnsRule = "ignore";
opts.EmptyLineRule = "read";
%queues = readtable(queuefile, opts);

%% load routes log

opts = delimitedTextImportOptions("NumVariables", 3);
opts.DataLines = [2, Inf];
opts.Delimiter = ",";
opts.VariableNames = ["ts", "node", "traversal"];
opts.VariableTypes = ["double", "double", "double"];
opts.ExtraColumnsRule = "ignore";
opts.EmptyLineRule = "read";
%traversals = readtable(routefile, opts);

%%traversals = traversals(traversals.ts >100 & traversals.ts < 1100,:);
%traversals = traversals(traversals.ts < 1000,:);

%%traversals.ts = traversals.ts -100;

%% load schedule log

opts = delimitedTextImportOptions("NumVariables", 3);
opts.DataLines = [2, Inf];
opts.Delimiter = ",";
opts.VariableNames = ["ts", "node", "resource"];
opts.VariableTypes = ["double", "double","double"];
opts.ExtraColumnsRule = "ignore";
opts.EmptyLineRule = "read";
%schedule = readtable(schedulefile, opts);


%% theoretical plots

% traffic ... bitrate

figure()
scatter3(thtraffic.t, thtraffic.src, thtraffic.dest);
xlabel("time [s]");
ylabel("src #");
zlabel("dest #");

%throughput

msize = 270;
asize = 140;

thT = [];

for ts = steps
   msg = thtraffic.t(thtraffic.t > ts & thtraffic.t <= ts+1);
   thT = [thT length(msg)*(msize+asize)];
end

figure()

scatter(steps, thT); %cr each second

dist = [];
for ts = steps
    a = size(thtraffic.t(thtraffic.t ==ts));
    dist = [dist a(1)];
end

figure()

histogram(dist)

% other throughput

% figure()
% hold on;
% th_throughput = [];
% pairs = [];
% times = [];
% 
% for i = 1:nbnode
%     for j = 1:nbnode
%         if i == j
%             continue
%         end
%             
%         th_throughput = [];
%         pairs = [];
%         times = [];
% 
%         for t= 1:max(traffic.ts)
%             winuser = traffic(traffic.ts >= t & traffic.ts < t+1,:);
%             wu = winuser.ts(winuser.src == i & winuser.dest == j);
%             pairs = [pairs i*nbnode+j];
%             th_throughput = [th_throughput 8*sum(wu*0+msize+asize)];
%             times = [times t];
%         end
%         scatter3(times,pairs,th_throughput);
%     end
% end
% 
% xlabel("time [s]");
% ylabel("pair i*nbnode +j");
% zlabel("throughput [bps]");

% connectivity and traffic

connectivity = [];
ratioconn = 0;
connectivitytraf = [];
ratioconntraf = 0;

for rt = steps
    
    is = mobility.i_nem_id(mobility.t == rt & mobility.pathloss <= pathlossthreshold);
    js = mobility.j_nem_id(mobility.t == rt & mobility.pathloss <= pathlossthreshold);
    
    srcs = thtraffic.src(thtraffic.t == rt);
    dests = thtraffic.dest(thtraffic.t == rt);
    
    con = 0;
    tot = 0;
    
    contraf = 0;
    tottraf = 0;
    
    G = graph(is,js);
    for s = 1:length(srcs)
        s = srcs(s); %convert index to source node
        for t = 1:length(dests)
            t = dests(t);
            if s ~= t %should not append with traffic
                s;
                t;
                p = shortestpath(G,s,t);
                if length(p)> 0
                    contraf = contraf +1;
                end
                tottraf = tottraf+1;
            end
        end 
    end
    for s = 1:24
        for t = 1:24
            if s ~= t %should not append with traffic
                s;
                t;
                p = shortestpath(G,s,t);
                if length(p)> 0
                    con = con +1;
                end
                tot = tot+1;
            end
        end 
    end
    
    rat = contraf/tottraf;
    connectivitytraf = [connectivitytraf rat];
    if rat == 1
        ratioconntraf = ratioconntraf +1;
    end
    
    rat = con/tot;
    connectivity = [connectivity rat];
    if rat == 1
        ratioconn = ratioconn +1;
    end
end

figure()
hold on;
plot(steps, connectivity)
scatter(steps, connectivitytraf)
plot([1 steps(end)], [mean(rmmissing(connectivitytraf)) mean(rmmissing(connectivitytraf))], 'LineWidth', 2, 'Color', 'r') %mean over all scenario

mvconn = movmean(connectivity, 10);
mvconntraf = movmean(connectivitytraf, 10);

%plot(steps, mvconn)
%plot(steps, mvconntraf)

ylim([0 1])

%title("connectivity of the pairs sd each 1s")
xlabel("time [s]")
ylabel("connectivity of the pairs sd")

legend(["connectivity", "connectivity + traffic", "mov mean connectivity", "mov mean connectivity + traffic"])


figure()
hold on;
for i= 1:24
    for j = i+1:24
        if i==j
            continue
        end
    i;
    j;
    pl = mobility.pathloss(mobility.i_nem_id == i & mobility.j_nem_id == j);
    ij = zeros(length(pl),1) + (i*24+j);
    ji = zeros(length(pl),1) + (j*24+i);
    scatter3(1:length(pl),ij, pl) 
    %scatter3(1:length(pl),ji,pl) 
    end
end

xlabel("time [s]")
ylabel("pairs i*24 +j  [#]")
zlabel("pathloss [dB]")

%% Measures plots

%% CR plots (take)

figure()
hold on;

for s = 1:nbnode
    %for t = 1:nbnode
    %    if s==t
    %        continue;
    %    end
    crs = [];
    for ts = steps
        cr = mean(traffic.cr(traffic.src == s & traffic.t > ts & traffic.t <= ts+1));
        crs = [crs cr];
        %trafficsd = traffic(traffic.src == s & traffic.dest == t,:);
    end
    
    scatter3(steps,zeros(length(steps),1)+s,crs);
end

title("CR per src");

%steps = 1:round(max(res.time))+1;
meancr = [];
mu = 7;
sigma = 1;
gm = gmdistribution(mu,sigma);
for ts = steps
   crs = traffic.cr(traffic.t > ts & traffic.t <= ts+1);
   acr = mean(crs)*100;
   %if acr < 100
   %    acr = acr - random(gm);
   %end
   meancr = [meancr acr];
end

figure()
hold on

scatter(steps, meancr); %cr each second

mvcr = movmean(meancr,10);
plot(steps,mvcr); %moving average
plot([1 steps(end)], [mean(rmmissing(meancr)) mean(rmmissing(meancr))], 'LineWidth', 2, 'Color', 'r') %mean over all scenario

ylim([0 100])

title("Completion Ratio sum(rx_{ack})/sum(tx_{msg}) each 1s","and moving average")
xlabel("time [s]")
ylabel("Completion Ratio (CR) [%]")

%legend(["RTT 1s-average", "moving average-10s"])


%% RTT

figure()
hold on;

for s = 1:nbnode
    %for t = 1:nbnode
    %    if s==t
    %        continue;
    %    end
    rtts = [];
    for ts = steps
        rtt = mean(traffic.rtt(traffic.src == s & traffic.t > ts & traffic.t <= ts+1));
        rtts = [rtts rtt/1000];
        %trafficsd = traffic(traffic.src == s & traffic.dest == t,:);
    end
    
    scatter3(steps,zeros(length(steps),1)+s,rtts);
end

title("RTT per src")

meanrtt = [];

for ts = steps
   rtts = traffic.rtt(traffic.t > ts & traffic.t <= ts+1 & traffic.cr == 1);
   meanrtt = [meanrtt mean(rtts)/1000];
end

figure()
hold on

scatter(steps, meanrtt); %cr each second

mvrtt = movmean(meanrtt,10);
plot(steps,mvrtt,'k'); %moving average
plot([1 steps(end)], [mean(rmmissing(meanrtt)) mean(rmmissing(meanrtt))], 'LineWidth', 2, 'Color', 'r') %mean over all scenario

ylim([0 max(meanrtt)])
ylim([0 30])

title("Round Trip Time each 1s","and moving average")
xlabel("time [s]")
ylabel("Round Trip Time (RTT) [s] ")

legend(["RTT 1s", "moving average"])


%% Throughput measured

meanTmsgTX = [];
meanTackTX = [];

for ts = steps
   msg = traffic.psize_B(traffic.t > ts & traffic.t <= ts+1); %& traffic.cr == 0
   msgandack = traffic(traffic.t > ts & traffic.t <= ts+1,:); % & traffic.cr == 1,:);

   meanTmsgTX = [meanTmsgTX 8*sum(msg)]; %over 1second ~ 24*8*270B/1s
   meanTackTX = [meanTackTX 8*(sum(msgandack.psize_B) + sum(rmmissing(msgandack.asize_B)))];
   
end

figure()
hold on

%scatter(steps, meanTmsgTX); %goodput msg each second
scatter(steps, meanTackTX); %goodput ack each second

mvgoodput = movmean(meanTackTX,10);
plot(steps,mvgoodput, 'r'); %moving average
%plot([1 steps(end)], [mean(rmmissing(meanrtt)) mean(rmmissing(meanrtt))], 'LineWidth', 2, 'Color', 'r') %mean over all scenario

ylim([0 max([meanTackTX])])
ylim([0 max([2*1e5])])

title("Throuhput each 1s (MSG+ACK if received)","and moving average")
xlabel("time [s]")
ylabel("Throughput R [bps] ")

legend(["R 1s", "moving average"])


%% CR * G

figure()
hold on

%divided by GFM

meanTackTXr = meanTackTX ./ (3/4 * 1e6);

CRG = ((meancr/100) .* (meanTackTXr)) * 100;
CRGmean = movmean(CRG, 10);

CRGm = mvgoodput .* mvcr;

%divided by 

plot(steps, CRG)
plot(steps, CRGmean, 'r')
%plot(steps, CRGm)

plot([1 steps(end)], [mean(rmmissing(CRGmean)) mean(rmmissing(CRGmean))], 'LineWidth', 2, 'Color', 'r') %mean over all scenario

ylim([0 100])
ylim([0 20])

title("CR*R ratio each 1s","and moving average")
xlabel("time [s]")
ylabel("CR * Rration [%] = CR * (msg+ack / 3/4 * 1e6) ")

legend(["CRG", "CRGmean"])
%legend(["CRG", "CRGmean", "CRGm"])


%% queue plots

N = max(cat(1,queues.src, queues.dest));

figure()
title("queue size versus time versus pairs");
xlabel("time [s]");
ylabel("pairs i*24+j");
zlabel("queue size [B]");
hold on;

plotbar = [];
minqtime = min(queues.ts);
queuetablecolormaptime = [];
queuetablecolormapsrc = [];
queuetablecolormapqueue = [];
for i = 1:N
    %for j = 1:N
    %    if i==j
    %        continue
    %    end
        %qsize = mergedqueue.len(mergedqueue.src == i & mergedqueue.dest == j & mergedqueue.len ~=0);
        %time = mergedqueue.time(mergedqueue.src == i & mergedqueue.dest == j & mergedqueue.len ~=0);
        
        %pairs = zeros(length(time),1)+ i*N + j;
        srci = queues(queues.src == i & queues.len ~=0,:);
        
        mint = min(srci.ts);
        maxt = max(srci.ts);
        
        time = [];
        queue = [];
        for rt = mint:1000:maxt %each 1s
            time = [time (rt-minqtime)/1000];
            queue = [queue sum(srci.len(srci.ts >= rt & srci.ts < rt+1000))];
        end
        
        plotbar = [plotbar mean(queue)];
        src = zeros(length(time),1)+i;
        scatter3(time,src,queue);
        queuetablecolormaptime = [queuetablecolormaptime  time];
        queuetablecolormapsrc = [queuetablecolormapsrc  src'];
        queuetablecolormapqueue = [queuetablecolormapqueue  queue];
   % end
end

figure()
title("Mean Queue size");
bar(plotbar);

xlabel("Node [#]");
ylabel("Queue size [B]");

legend(["Round Robin Schedule"]); %,"Suboptimal schedule"])


figure()
title("queue size versus time versus pairs COLOR");
scatter3(queuetablecolormaptime, queuetablecolormapsrc, queuetablecolormapqueue,[], queuetablecolormapqueue);

xlabel("time")
ylabel("node")
zlabel("queue size [B]")

view([0 90])

colormap(col);
colorbar;
caxis([0 max(queuetablecolormapqueue)]);

%% node view graph



%% number of traversals

figure()
title("Number of traversals from the routing tables");

trav = traversals.traversal;
trav(trav<15)=nan; 

scatter3(traversals.ts, traversals.node, trav,[], trav);

xlabel("time")
ylabel("node")
zlabel("traversals")

view([0 90])

ylim([0 25])

colormap(col);
colorbar;
caxis([0 max(traversals.traversal)]);


%% ressource allocated on nodes (should reflect the number of traversals)

figure()
scatter3((schedule.ts-min(schedule.ts))*0.8, schedule.node, schedule.resource,[], schedule.resource, '+');
%scatter(schedule.ts, schedule.node);

xlabel("time")
ylabel("node")
zlabel("resource")

view([0 0])

colormap(col);
colorbar;
caxis([0 max(schedule.resource)]);

%heatmap

schedule.t = floor(schedule.ts-min(schedule.ts));
%hms = [];
for i = 0:2:545
    i
    figure()
   

   %slots = sortrows(slots,'node','ascend');
   hms = [];
   for s = 1: 24
       slots = schedule(schedule.t == i,:);
       
       res = slots.resource(slots.node == s);
       ti = 0;
       si = 1;
       while length(res) == 0 % search for the proximity
           
           slots = schedule(schedule.t == i + si*ti,:);
           res = slots.resource(slots.node == s);
           ti = ti+1;
           si = si*-1;
       end
       if length(res)>5
           res = res(1:5);
       end
       res = padarray(res, 5-length(res), NaN, 'post')';
       
       hms = [hms; res];
       
   end 
   heatmap(hms, 'Colormap', jet);
   title(strcat("schedule at ts=",num2str(i), "s"))
   caxis([0 24])
   xlabel("slots ID [#]")
   ylabel("slots on node [#]")
   
end


