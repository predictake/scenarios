import datetime
from pathlib import Path
from eelfile import write_eel_file
from geopy import distance
import math
from scipy.special import comb
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.animation import FuncAnimation
import json
import seaborn as sns
sns.set_theme(style="darkgrid", palette="pastel")

# Folder parameters
now = datetime.datetime.now()
time_string = now.strftime("%Y-%m-%d_%H-%M-%S")

folder_main = f"results_{time_string}/"
folder_plots = folder_main + "plots"
folder_csv = folder_main + "csv"

# create folders
Path(folder_plots).mkdir(parents=True, exist_ok=True)
Path(folder_csv).mkdir(parents=True, exist_ok=True)

##################################################################################################################
# Generate the paths x,y
##################################################################################################################

# https://stackoverflow.com/questions/5837572/generate-a-random-point-within-a-circle-uniformly


def generate_random_point_within_circle(zone):
    ang = np.random.uniform(0, 1) * 2 * math.pi
    hyp = (np.random.uniform(0, 1)) * zone["radius"]
    adj = np.cos(ang) * hyp
    opp = np.sin(ang) * hyp
    x = zone["x"] + adj
    y = zone["y"] + opp
    return {"x": x, "y": y}

# https://stackoverflow.com/questions/12643079/b%C3%A9zier-curve-fitting-with-scipy


def bernstein_poly(i, n, t):
    """
     The Bernstein polynomial of n, i as a function of t
    """

    return comb(n, i) * (t**(n-i)) * (1 - t)**i


def bezier_curve(points, nTimes=1002):
    """
       Given a set of control points, return the
       bezier curve defined by the control points.

       points should be a list of lists, or list of tuples
       such as [ [1,1], 
                 [2,3], 
                 [4,5], ..[Xn, Yn] ]
        nTimes is the number of time steps, defaults to 1000

        See http://processingjs.nihongoresources.com/bezierinfo/
    """

    nPoints = len(points)
    xPoints = np.array([p[0] for p in points])
    yPoints = np.array([p[1] for p in points])

    t = np.linspace(0.0, 1.0, nTimes)

    polynomial_array = np.array(
        [bernstein_poly(i, nPoints-1, t) for i in range(0, nPoints)])

    xvals = np.dot(xPoints, polynomial_array)
    yvals = np.dot(yPoints, polynomial_array)

    return xvals, yvals


def generate_random_value(x1, x2):
    return x1 + (x2 - x1) * np.random.rand()


def generate_coordinates_for_nem(nemID, df, bezier_points, depart_zone, arrival_zone):
    loc_depart = generate_random_point_within_circle(depart_zone)
    loc_arrival = generate_random_point_within_circle(arrival_zone)

    pd.concat([df, pd.DataFrame([[int(0), nemID, loc_depart["x"], loc_depart["y"],
              100]], columns=["timestamp", "nemID", "lat", "long", "alt"])])

    depart = np.array([[loc_depart["x"], loc_depart["y"]]])
    arrival = np.array([[loc_arrival["x"], loc_arrival["y"]]])

    points = np.concatenate((depart, bezier_points, arrival)) if len(
        bezier_points) > 0 else np.concatenate((depart, arrival))

    # at the moment the nb of second corresponds to the number of points
    xvals, yvals = bezier_curve(points, nTimes=1002)

    for idx, val in enumerate((zip(reversed(xvals), reversed(yvals)))):
        noise = np.random.uniform(-5, 5, size=(1, 2))
        val += noise
        df.loc[len(df)] = [int(idx), nemID, val[0][0], val[0][1], 100]

    # add a row with the arrival location
    df.loc[len(df)] = [int(len(xvals)), nemID,
                       loc_arrival["x"], loc_arrival["y"], 100]


def generate_eel_dynamic(nb_nems, group_attributions, group_destinations, nb_points_bezier, depart_zones, arrival_zones, box_size):
    df = pd.DataFrame(columns=["timestamp", "nemID", "x", "y", "alt"])

    bezier_curve_points = []

    # generate the bezier curve points
    nem_id = 0
    for i1, attributions in enumerate(group_attributions):
        depart_zone = depart_zones[i1]
        for i2, group_size in enumerate(attributions):
            dest = group_destinations[i1][i2]
            arrival_zone = arrival_zones[dest]
            if nb_points_bezier > 0:
                bezier_points = np.random.rand(
                    nb_points_bezier, 2)*np.array([box_size[0], box_size[1]])
                # bezier_points = np.empty((nb_points_bezier,2))
                # x_min = -50
                # x_max = arrival_zone["x"] + arrival_zone["radius"]
                # y_min = arrival_zone["y"] - arrival_zone["radius"]
                # y_max = 1050
                # for i in range(nb_points_bezier):
                #     # x = generate_random_value(x_min, x_max)
                #     # assert x_min <= x <= x_max
                #     # x_min = x
                #     x = np.random.rand()*np.array(box_size[0])
                #     y = generate_random_value(y_min, y_max)
                #     assert -50 <= y <= y_max
                #     y_max = y
                #     # y = np.random.rand()*np.array(box_size[1])
                #     bezier_points[i,:] = [x, y]
            else:
                bezier_points = []

            # generate the coordinates for the nem
            for i in range(group_size):

                # noise = np.random.uniform(-50, 50, size=bezier_points.shape)
                # bezier_points += noise
                bezier_curve_points.append(bezier_points)

                generate_coordinates_for_nem(
                    nem_id+1, df, bezier_points, depart_zone, arrival_zone)
                nem_id += 1

    return df, bezier_curve_points

##################################################################################################################
# Plotting functions: paths, bezier curves, etc.
##################################################################################################################


def plot_paths_depart_arrival(df, depart_zones, arrival_zones, box_size):

    plt.figure(figsize=(12, 18))
    plt.xlim([-50, box_size[0]])
    plt.ylim([-50, box_size[1]])

    # plot the departure zones
    for idx, d_zone in enumerate(depart_zones):
        plt.plot(d_zone["x"], d_zone["y"], "ro")
        plt.plot([d_zone["x"]], [d_zone["y"]], 'o',
                 ms=d_zone["radius"], mec='r', mfc='none')
        plt.text(d_zone["x"] - 50, d_zone["y"] + 2.5 *
                 d_zone["radius"], f"Departure {idx}", c="r")

    # plot the arrival zones
    for idx, a_zone in enumerate(arrival_zones):
        plt.plot(a_zone["x"], a_zone["y"], "bo")
        plt.plot([a_zone["x"]], [a_zone["y"]], 'o',
                 ms=a_zone["radius"], mec='b', mfc='none')
        plt.text(a_zone["x"] - 50, a_zone["y"] + 2.5 *
                 d_zone["radius"], f"Arrival {idx}", c="b")

    # save the figure
    plt.savefig(f"{folder_plots}/depart_arrival_zones.png")
    plt.close()


def plot_paths_v2(df, depart_zones, arrival_zones, box_size):

    plt.figure(figsize=(12, 18))
    plt.xlim([-50, box_size[0]])
    plt.ylim([-50, box_size[1]])

    # plot the departure zones
    for idx, d_zone in enumerate(depart_zones):
        plt.plot(d_zone["x"], d_zone["y"], "ro")
        plt.plot([d_zone["x"]], [d_zone["y"]], 'o',
                 ms=d_zone["radius"], mec='r', mfc='none')
        plt.text(d_zone["x"] - 50, d_zone["y"] + 2.5 *
                 d_zone["radius"], f"Departure {idx}", c="r")

    # plot the arrival zones
    for idx, a_zone in enumerate(arrival_zones):
        plt.plot(a_zone["x"], a_zone["y"], "bo")
        plt.plot([a_zone["x"]], [a_zone["y"]], 'o',
                 ms=a_zone["radius"], mec='b', mfc='none')
        plt.text(a_zone["x"] - 50, a_zone["y"] + 2.5 *
                 d_zone["radius"], f"Arrival {idx}", c="b")

    # there is sometime artefact when using lineplot
    # sns.lineplot(x="x", y="y", data=df, hue="nemID")
    sns.scatterplot(x="x", y="y", data=df, hue="nemID", s=8, alpha=1)

    data = df[df["timestamp"] == 0]
    # Annotate label points with nemID
    for nem_id, row in data.iterrows():
        plt.annotate(int(row["nemID"]), (row["x"], row["y"]))

    # save the figure
    plt.savefig(f"{folder_plots}/paths.png")
    plt.close()


def plot_path_isolate_nem(nemID, df, depart_zones, arrival_zones, bezier_curve_points):
    data = df[df["nemID"] == nemID]

    for idx, d_zone in enumerate(depart_zones):
        plt.plot(d_zone["x"], d_zone["y"], "ro")
        plt.plot([d_zone["x"]], [d_zone["y"]], 'o',
                 ms=d_zone["radius"], mec='r', mfc='none')
        plt.text(d_zone["x"] - 50, d_zone["y"] + 2.5 *
                 d_zone["radius"], f"Departure {idx}", c="r")

    for idx, a_zone in enumerate(arrival_zones):
        plt.plot(a_zone["x"], a_zone["y"], "bo")
        plt.plot([a_zone["x"]], [a_zone["y"]], 'o',
                 ms=a_zone["radius"], mec='b', mfc='none')
        plt.text(a_zone["x"] - 50, a_zone["y"] + 2.5 *
                 d_zone["radius"], f"Arrival {idx}", c="b")

   # create df with columns : nemID, pointID, x, y
    df_bezier_points = pd.DataFrame(columns=["pointID", "nemID", "x", "y"])
    for idx, bezier_points in enumerate(bezier_curve_points):
        for idx1, point in enumerate(bezier_points):
            df_bezier_points.loc[len(df_bezier_points)] = [
                idx1, int(idx+1), point[0], point[1]]

    # filter df_bezier_points to only keep points for this nem
    df_bezier_points = df_bezier_points[df_bezier_points["nemID"] == nemID]

    ax = sns.scatterplot(x="x", y="y", data=df_bezier_points)
    for i, point in df_bezier_points.iterrows():
        ax.text(point["x"], point["y"] + 20, point["pointID"])

    # sns.lineplot(x="lat", y="long", data=df, hue="nemID")
    # plt.plot(data["x"], data["y"], c="k", alpha=0.1)
    sns.scatterplot(x="x", y="y", data=data, hue="nemID")


def plot_paths_isolate_nem(nb_nems, df, depart_zones, arrival_zones, bezier_curve_points):
    for nemID in range(1, nb_nems+1):
        plt.figure(figsize=(12, 18))

        plot_path_isolate_nem(nemID, df, depart_zones,
                              arrival_zones, bezier_curve_points)
        # plt.show()
        plt.savefig(f"{folder_plots}/paths_nem_{nemID}.png")
        plt.close()

##################################################################################################################
# Plotting functions: speed
##################################################################################################################


def plot_speed(df, nemID):
    plt.figure(figsize=(30, 10))

    sns.lineplot(x="timestamp", y="speed_km/h",
                 label=f"nemID {nemID}", data=df)

    avg_speed = df["speed_km/h"].mean()
    plt.plot(df["timestamp"], [avg_speed]*len(df["timestamp"]), "r--")

    # plt.show()
    plt.savefig(f"{folder_plots}/speed_nemID_{nemID}.png")
    plt.close()


def plot_speeds(df):
    for nemID in df["nemID"].unique():
        data = df[df["nemID"] == nemID]
        plot_speed(data, nemID)

##################################################################################################################
# Convert x,y to lat,long
##################################################################################################################


def quickanddurtyshiftcoordinates1(x, y):
    # https://gis.stackexchange.com/questions/2951/algorithm-for-offsetting-a-latitude-longitude-by-some-amount-of-meters
    lat = 51.0
    lon = 0.0

    R = 6378137

    # offsets in meters
    dn = x
    de = y

    dLat = dn / R
    dLon = de / (R * math.cos(math.pi * lat / 180))

    latO = lat + dLat * 180 / math.pi
    lonO = lon + dLon * 180 / math.pi

    return latO, lonO


def quickanddurtyshiftcoordinates(x, y):
    lat = y / 111111.
    lon = x / (111111. * math.cos(lat))
    return lat, lon


def convert_to_gps(df):
    data = df.copy()
    data = data.sort_values(by=["timestamp", "nemID"])
    data["(lat,long)"] = data.apply(
        lambda row: quickanddurtyshiftcoordinates(row["x"], row["y"]), axis=1)

    data["lat"] = data["(lat,long)"].apply(lambda row: row[0])
    data["long"] = data["(lat,long)"].apply(lambda row: row[1])

    data = data.drop(columns=["(lat,long)"])

    return data

##################################################################################################################
# Speed calculation
##################################################################################################################


def calc_speed(df, nb_nems):

    df_speed = pd.DataFrame(columns=["timestamp", "nemID", "speed_m/s"])

    for nemID in range(1, nb_nems+1):
        data = df[df["nemID"] == nemID].copy()
        data["prev_x"] = data["x"].shift(1)
        data["prev_y"] = data["y"].shift(1)
        data["speed_m/s"] = data.apply(lambda row: math.sqrt(
            (row["x"] - row["prev_x"])**2 + (row["y"] - row["prev_y"])**2), axis=1)
        to_add = data[["timestamp", "nemID", "speed_m/s"]]
        df_speed = pd.concat([df_speed, to_add])

    df_speed["speed_km/h"] = df_speed["speed_m/s"] * 3.6
    return df_speed

##################################################################################################################
# Distance calculation
##################################################################################################################


def calc_distance_between_nems(df, nb_nems):
    df1 = df[["timestamp", "nemID", "lat", "long"]].copy()
    df1.rename(columns={"nemID": "nemID1"}, inplace=True)

    df2 = df[["timestamp", "nemID", "lat", "long"]].copy()
    df2.rename(columns={"nemID": "nemID2"}, inplace=True)

    df1 = df1.merge(df2, on=["timestamp"])

    df1.rename(columns={"lat_x": "lat1", "long_x": "long1",
               "lat_y": "lat2", "long_y": "long2"}, inplace=True)
    df1["distance"] = df1.apply(lambda row: distance.geodesic(
        (row["lat1"], row["long1"]), (row["lat2"], row["long2"])).m, axis=1)

    df1.drop(columns=["lat1", "long1", "lat2", "long2"],
             inplace=True, errors="ignore")
    df1['idx'] = df1.groupby(["timestamp", "nemID1"]).cumcount()

    df_distances = df1.pivot(
        index=["timestamp", "nemID1"], columns="idx", values="distance")
    df_distances.columns = [nemID for nemID in range(1, nb_nems + 1)]

    df1.drop(columns=["idx"], inplace=True, errors="ignore")
    return df_distances

##################################################################################################################
# Pathloss calculation
##################################################################################################################

# default values were taken form urban.py


def pathloss_calc(distance, lo=20, alpha=30): #lo=25, alpha=40
    return lo + alpha * np.log10(distance) if distance > 0 else 40
    
def longley_rice():
    pass

##################################################################################################################
# format df for write_eel
##################################################################################################################


def format_for_write_eel(df_gps, df_pathloss):

    eelinfo = {'location': [], 'pathloss': []}

    df_gps_cp = df_gps.copy()
    df_pathloss_cp = df_pathloss.copy()

    # format the location
    df_gps_cp["location"] = df_gps.apply(lambda row: [int(row["timestamp"]), int(
        row["nemID"]), row["lat"], row["long"], row["alt"]], axis=1)

    # format the pathloss
    df_pathloss_cp = df_pathloss_cp.drop(columns=["pathloss"], errors="ignore")
    to_remove = df_pathloss_cp.columns[-1]
    df_pathloss_cp["pathloss"] = df_pathloss_cp.apply(lambda row: [int(row["timestamp"]), int(row["nemID1"]), [
                                                      [nemID2, row[nemID2]] for nemID2 in df_pathloss.columns[2:] if (nemID2 not in ["timestamp", "nemID1"]) and (row["nemID1"] < nemID2)]], axis=1)

    df_pathloss_cp = df_pathloss_cp[df_pathloss_cp["nemID1"] != to_remove]

    eelinfo["location"] = df_gps_cp["location"].tolist()
    eelinfo["pathloss"] = df_pathloss_cp["pathloss"].tolist()

    return eelinfo

##################################################################################################################
# Summary
##################################################################################################################


def summary(nb_nems, nb_points_bezier, depart_zones, arrival_zones, group_attributions, group_destinations):
    with open(f"{folder_main}summary.md", "w") as f:
        f.write(f"# Summary\n")
        f.write(f"* nb_nems: {nb_nems}\n")
        f.write(f"* nb_points_bezier: {nb_points_bezier}\n")
        f.write(f"* nb_depart_zones: {len(depart_zones)}\n")
        f.write(f"* nb_arrival_zones: {len(arrival_zones)}\n")
        nb_groups = len(np.concatenate(group_attributions).ravel())
        f.write(f"* nb_groups: {nb_groups}\n")

        f.write("\n")

        # make a table
        f.write(f"| group | nb_nems | Departure zone | Arrival zone | \n")
        f.write(f"| --- | --- | --- | --- | \n")
        grp_id = 1
        for idx1, starting_pos in enumerate(group_attributions):
            for idx2, size in enumerate(starting_pos):
                f.write(
                    f"{grp_id} | {size} | {idx1} | {group_destinations[idx1][idx2]} |\n")
                grp_id += 1

##################################################################################################################
# Animation
##################################################################################################################


def plot_paths_background(df, depart_zones, arrival_zones, box_size):

    # plot the departure zones
    for idx, d_zone in enumerate(depart_zones):
        plt.plot(d_zone["x"], d_zone["y"], "ro")
        plt.plot([d_zone["x"]], [d_zone["y"]], 'o',
                 ms=d_zone["radius"], mec='r', mfc='none')
        plt.text(d_zone["x"] - 50, d_zone["y"] + 2.5 *
                 d_zone["radius"], f"Departure {idx}", c="r")

    # plot the arrival zones
    for idx, a_zone in enumerate(arrival_zones):
        plt.plot(a_zone["x"], a_zone["y"], "bo")
        plt.plot([a_zone["x"]], [a_zone["y"]], 'o',
                 ms=a_zone["radius"], mec='b', mfc='none')
        plt.text(a_zone["x"] - 50, a_zone["y"] + 2.5 *
                 d_zone["radius"], f"Arrival {idx}", c="b")

    # there is sometime artefact when using lineplot
    # sns.lineplot(x="x", y="y", data=df, hue="nemID")
    plt.size = (8, 12)
    sns.scatterplot(x="x", y="y", data=df, hue="nemID", s=8, alpha=0.5)

    data = df[df["timestamp"] == 0]
    # Annotate label points with nemID
    for nem_id, row in data.iterrows():
        plt.annotate(int(row["nemID"]), (row["x"], row["y"]))


def generate_animation(df, depart_zones, arrival_zones, box_size):
    # look into this post, maybe solution to only display 1 point at a time
    # https://stackoverflow.com/questions/35521545/how-to-animate-a-moving-scatter-point
    fig = plt.figure(figsize=(12, 24))

    axes = fig.add_subplot(111)
    axes.set_xlim(-50, box_size[0])
    axes.set_ylim(-50, box_size[1])

    plot_paths_background(df, depart_zones, arrival_zones, box_size)
    point, = axes.plot(df[df["timestamp"] == 0]["x"],
                       df[df["timestamp"] == 0]["y"], 'go')

    def init():
        data = df[df["timestamp"] == 0]
        sns.scatterplot(x="x", y="y", data=data, hue="nemID",
                        legend=False, palette="dark")

    def animate(i):
        data = df[df["timestamp"] == i]
        # set title to timestamp
        fig.suptitle(f"Timestamp {str(i+1)}")
        point.set_data([data["x"]], [data["y"]])
        return point

    anim = animation.FuncAnimation(fig, animate, init_func=init, frames=range(
        0, 1003, 10), repeat=False, interval=0.1)

    # plt.show()
    anim.save(f'{folder_main}eel_dynamic.gif', writer='Pillow', fps=30)


def main(box_size, depart_zones, arrival_zones, group_attributions, group_destinations, nb_points_bezier):
    nb_nems = np.concatenate(group_attributions).ravel().sum()
    assert len(depart_zones) == len(group_attributions)

    print("Generating summary...", end="", flush=True)
    summary(nb_nems, nb_points_bezier, depart_zones,
            arrival_zones, group_attributions, group_destinations)
    print("Done", flush=True)

    print("Generating NEMs path...", end="", flush=True)
    df, bezier_curve_points = generate_eel_dynamic(nb_nems=nb_nems,
                                                   group_attributions=group_attributions,
                                                   group_destinations=group_destinations,
                                                   nb_points_bezier=nb_points_bezier,
                                                   depart_zones=depart_zones,
                                                   arrival_zones=arrival_zones,
                                                   box_size=box_size)
    print("Done", flush=True)

    print("Generating plots...", end="", flush=True)
    plot_paths_v2(df, depart_zones, arrival_zones, box_size)
    plot_paths_depart_arrival(df, depart_zones, arrival_zones, box_size)

    # save df and bezier_curve_points
    df.to_csv(f"{folder_csv}/df_paths_x_y.csv")
    np.save(f"{folder_csv}/bezier_curve_points.npy", bezier_curve_points)

    plot_paths_isolate_nem(nb_nems=nb_nems, df=df, depart_zones=depart_zones,
                           arrival_zones=arrival_zones, bezier_curve_points=bezier_curve_points)
    print("Done", flush=True)

    print("Generating animation...", end="", flush=True)
    generate_animation(df, depart_zones, arrival_zones, box_size)
    print("Done", flush=True)

    print("Converting to GPS...", end="", flush=True)
    df_gps = convert_to_gps(df)
    df_gps.to_csv(f"{folder_csv}/df_path_lat_long.csv", index=False)
    print("Done", flush=True)

    print("Calculating speed...", end="", flush=True)
    df_speed = calc_speed(df_gps, nb_nems)
    df_speed.to_csv(f"{folder_csv}/df_speed.csv", index=False)
    print("Done", flush=True)

    print("Plotting speed...", end="", flush=True)
    plot_speeds(df_speed)
    print("Done", flush=True)

    print("Calculating distance between nems...", end="", flush=True)
    df_distances = calc_distance_between_nems(df_gps, nb_nems)
    df_distances.to_csv(f"{folder_csv}/df_distances.csv", index=False)
    print("Done", flush=True)

    print("Calculating pathlosses...", end="", flush=True)
    # apply pathloss_calc to all cell of df_distances
    df_pathloss = df_distances.applymap(pathloss_calc).reset_index()

    # save df_pathloss
    df_pathloss.to_csv(f"{folder_csv}/df_pathloss.csv", index=False)
    print("Done", flush=True)

    print("Generating EEL file...", end="", flush=True)
    eelinfo = format_for_write_eel(df_gps=df_gps, df_pathloss=df_pathloss)
    write_eel_file(f"{folder_main}{nb_nems}_dynamic.eel",
                   eelinfo, schedules=[])
    print("Done", flush=True)


# main
if __name__ == "__main__":
    # # group_attributions = [[depart_zones 0], [depart_zones 1], [depart_zones 2]]
    # # eg, [[2, 2], [2], [3]]
    # #   => [2, 4] : 2 groups start in depart_zones 0, 1 group of size 2, 1 group of size 3
    # #   => [2] : 1 group of size 2 starts in depart_zones 1
    # #   => [3] : 1 group of size 3 starts in depart_zones 2
    # # group_attributions = np.array([[10], [4], [4, 3], [3]], dtype=object)
    # group_attributions = np.array([[1], [1], [1, 1], [1]], dtype=object)
    # # destination for each group
    # group_destinations = [[0], [0], [1, 2], [2]]

    path_to_config = "eel_dynamic_config.json"

    with open(path_to_config, "r") as f:
        config = json.load(f)
        config["group_attributions"] = np.array(
            config["group_attributions"], dtype=object)
        main(**config)
