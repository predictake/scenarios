import re
import sys
sys.path.append('../')
# from conf.config import *


class EEL_Attribute(object):

    def __init__(self):  # attributes
        self.location = []  # TODO lat,lon,alt
        self.pathloss = {}


class EEL_Nem(object):

    def __init__(self):  # nem
        self.timestamp = {}

    def test_attributes(self, time):
        if time not in self.timestamp.keys():
            attribute = EEL_Attribute()
            self.timestamp[time] = attribute

    def add_location(self, time, location):
        self.test_attributes(time)

        spl = location.split(',')
        for arg in spl:
            self.timestamp[time].location.append(float(arg))

    def add_pathloss(self, time, pl):
        self.test_attributes(time)

        for nem in pl:
            p = nem.split(':')
            if len(p) >= 2:
                n_p = p[1].split(',')
                if len(n_p) >= 2:
                    self.timestamp[time].pathloss[int(n_p[0])] = float(n_p[1])
                else:
                    print("[W] pathloss not conform")
            else:
                print("[W] pathloss not conform")


class EEL_File(object):
    def __init__(self):  # file
        self.nems = {}

    def _test_nem(self, nem, time):
        if nem not in self.nems.keys():
            nem_obj = EEL_Nem()
            self.nems[nem] = nem_obj

    def add_pathloss_nem(self, time, nem, pl):
        self._test_nem(nem, time)
        self.nems[nem].add_pathloss(time, pl)

    def add_location_nem(self, time, nem, location):
        self._test_nem(nem, time)
        self.nems[nem].add_location(time, location)

    def add_nem_info(self, line):
        ret = False
        sp_line = line.split(' ')
        if len(sp_line) >= 4:
            time = int(sp_line[0])
            nem = int(sp_line[2].split(':')[1])
            if sp_line[3] == "location":
                self.add_location_nem(time, nem, sp_line[5])
            elif sp_line[3] == "pathloss":
                self.add_pathloss_nem(time, nem, sp_line[4:])
                ret = True
            else:
                print("[W] the function is not supported", line)

        else:
            print("[W] miss an entry", line)

        return ret


def read_eel_file(eel_file_name):
    eel_file_ob = EEL_File()
    file = open(eel_file_name, 'r')
    lines = file.readlines()
    for line in lines:
        line = re.sub('[^A-Za-z0-9.,: ]+', '', line)
        eel_file_ob.add_nem_info(line)

    return eel_file_ob


def write_eel_file(eel_file_name, obj, schedules=[]):
    file = open(eel_file_name, 'w')
    for k, v in obj.items():
        if k == 'location':
            for l in v:
                file.write('{}  nem:{} location gps {},{},{}\n'.format(
                    l[0], l[1], l[2], l[3], l[4]))
        elif k == 'pathloss':

            for pl in v:
                pathloss = ""
                for p in pl[2]:
                    pathloss += " nem:{},{}".format(p[0], p[1])
                file.write('{}  nem:{} pathloss'.format(
                    pl[0], pl[1]) + pathloss + '\n')

    for schedule in schedules:
        file.write(schedule + '\n')
    file.close()


if __name__ == '__main__':
    folder = 'C:/Users/lte/PycharmProjects/Emane/Results/generator/static/07012020_1_1_company1_TDMA_MD_custom_pcr/'
    file = 'generated_1.eel'
    obj = read_eel_file(folder + file)

    lat = []
    lon = []

    for n, nem in obj.nems.items():
        for t, att in nem.timestamp.items():  # lat, lon, al
            lat.append(att.location[0])
            lon.append(att.location[1])

    print('lon', lon)
    print('lat', lat)
