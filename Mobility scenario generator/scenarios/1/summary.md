# Summary
* nb_nems: 5
* nb_points_bezier: 2
* nb_depart_zones: 4
* nb_arrival_zones: 3
* nb_groups: 5

| group | nb_nems | Departure zone | Arrival zone | 
| --- | --- | --- | --- | 
1 | 1 | 0 | 0 |
2 | 1 | 1 | 0 |
3 | 1 | 2 | 1 |
4 | 1 | 2 | 2 |
5 | 1 | 3 | 2 |
