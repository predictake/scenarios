folder ="X:\Results\laptop\experiments\results_all_laptops\bk_measure\LaptopShow-6nodesolsrsinrttakeA2A1msg\20230823-1530"
    
if exist('rssi','var') == 0
    opts = delimitedTextImportOptions("NumVariables", 12);
    opts.DataLines = [2, Inf];
    opts.Delimiter = ",";
    opts.VariableNames = ["mode", "time", "us", "ts", "rate", "rssi1", "rssi2", "da", "sa", "ra", "ta", "bssid", "nodei"];
    opts.VariableTypes = ["double", "datetime", "double","double", "double", "double", "double", "double", "double", "double", "string", "categorical", "double"];
    opts.ExtraColumnsRule = "ignore";
    opts.EmptyLineRule = "read";
    opts = setvaropts(opts, "ta", "WhitespaceRule", "preserve");
    opts = setvaropts(opts, ["ta", "bssid"], "EmptyFieldRule", "auto");
    opts = setvaropts(opts, "time", "InputFormat", "HH:mm:ss.SSSSSS");
    opts = setvaropts(opts, ["us", "rssi1", "rssi2"], "TrimNonNumeric", true);
    opts = setvaropts(opts, ["us", "rssi1", "rssi2"], "ThousandsSeparator", ",");
    
    rssi = readtable(folder+"\log\rssi.csv", opts);
    %rssi2 = readtable(folder+"\log\2\rssi.csv", opts);
    %rssi3 = readtable(folder+"\log\3\rssi.csv", opts);
    %rssi4 = readtable(folder+"\log\4\rssi.csv", opts);
    %rssi5 = readtable(folder+"\log\5\rssi.csv", opts);
    %rssi6 = readtable(folder+"\log\6\rssi.csv", opts);

    %rssis = {rssi1,rssi2,rssi3,rssi4,rssi5,rssi6};

end

PTx = 20 %dBm
Ga = 4 %dBi

rmode = [3,5,6] 
rmode = [6] %use mode 6 to reproduce the scenario in emulation 

savepathloss = []

%use plotmeasure.m of theoryandmeasure.m to load the correct traffic and
%substract to rssi to create rssi.t
rssi = rssi(rssi.t>0,:);

refset = 0;
for node = 1:6 %receiver
    %rssi = rssis{node};
    for i = 1:length(rmode)
        mode = rmode(i);
        r = rssi(rssi.mode==rmode(i) & rssi.nodei == node,:);

        if isnan(r.sa(1))
            r.sender = r.ta;
        else
            r.sender = r.sa;
        end

        figure()
        title(strcat("Node: ",num2str(node)," and mode: ",num2str(rmode(i))))
        hold on;

        xlabel("time [s]")
        ylabel("sender [#]")
        %yyaxis left
        %zlabel('RSSI [dBm] / RATE[Mbps]')
        zlabel('pathloss [dB]')
        %yyaxis right
        %zlabel('RATE [Mbps]')

        for i = 1:6 %transmitter
            if i == node
                continue
            end
            
            rs = r(r.sender==i ,:);
            si = size(rs);
            
            %pathloss = PTx + 2*Ga - max(rs.rssi1, rs.rssi2);
            pathloss = 40 - max(rs.rssi1, rs.rssi2); %40dB fitted to measure 115dB (anglovaB POR 90%) -75 (wifi no connection) = 40dB
            
         	if 1
                %plot3(rs.time,rs.sender,max(rs.rssi1,rs.rssi2));
                %plot3(rs.time,rs.sender,rs.rssi1-rs.rssi2);
                %plot3(rs.time,rs.sender,rs.rate);

                scatter3(seconds(rs.time-min(rs.time)),rs.sender,pathloss);

                X = convertTo(rs.time,'epochtime','Epoch','2001-01-01','TicksPerSecond',1000);
                %savepathloss = [savepathloss; X rs.sender rs.nodei pathloss];
            end

            %sampling
            %TT = timetable(rs.time, rs.sender, rs.nodei, pathloss);
            Time = rs.time;
            Pathloss = pathloss;
            TT = timetable(Time, Pathloss);
            TT = retime(TT,unique(TT.Time),"mean"); % first clean duplicate, takes the mean
            %secsample = retime(TT,'secondly','mean');
            %secsample= retime(TT2,'secondly','fillwithconstant','Constant',300);
            secsample = retime(TT,'regular','linear','SampleRate',5); %5Hz -> 200ms or 'TimeStep',0.2 timestep?
            %ms = datetime('0.2', 'Format', 'ss.SSS');
            %secsample = retime(TT,'regular','linear','TimeStep',1); %5Hz -> 200ms or 'TimeStep',0.2 timestep?
                        
            %plot3(secsample.Time,secsample.Var1,secsample.Var3);

            secsample = secsample(2:end,:);

            if refset == 0
                refsample = secsample;
                refset = 1;
                pl = secsample.Pathloss;
            else
                secsample = synchronize(secsample, refsample, 'regular','mean','SampleRate',5);
                pl = secsample.Pathloss_secsample;
            end

            %pl(isnan(pl)) = 150; % or neareast value
            source = zeros(length(pl),1) + i;
            destination = zeros(length(pl),1) + node;
            plot3(seconds(secsample.Time-min(secsample.Time)),source,pl);
            
            X = convertTo(secsample.Time,'epochtime','Epoch','2001-01-01','TicksPerSecond',1000);
            %savepathloss = [savepathloss; X source destination pl];
            if length(savepathloss) == 0 %should find the least number of point first...
                savepathloss = [savepathloss pl];
                savetime = secsample.Time;
            else
                savepathloss = [savepathloss pl(1:length(savepathloss))];
            end
        end
        view([-45,10]);
       
        %legend(["max(rssi1)","rate1","max(rssi2)","rate2","max(rssi3)","rate3","max(rssi4)","rate4","max(rssi5)","rate5","max(rssi6)","rate6"])
    end
end
TT = timetable(savetime, savepathloss);
finalpathloss = retime(TT,'regular','linear','SampleRate',5);

%% pay attention to negative pathloss value due to the linear extrapolation at the end of the scenario (should detect the link with minimal number of sample before extrapolation...)
dlmwrite('pathloss.csv', finalpathloss.savepathloss, 'delimiter', ',','precision', 12);

%savepathloss = sortrows(savepathloss,1,'ascend');
%T = array2table(savepathloss);
%T.Properties.VariableNames(1:4) = {'time_ms','source','destination', 'pathloss'};
%writetable(T,'file1.csv')
%dlmwrite('pathloss.csv', savepathloss, 'delimiter', ',','precision', 12);