import datetime

"""
other  : 2022-11-28 17:15:03.575931 119505266us tsft 1.0 Mb/s 2462 MHz 11b -34dBm signal -34dBm signal antenna 0 Protected 314us DA:01:00:5e:00:00:fb SA:c4:e9:84:1d:a2:bc BSSID:2a:d3:8a:17:b0:d8 Data IV:8093c Pad 0 KeyID 0
bit 15 : 2022-11-28 17:15:05.599418 1.0 Mb/s [bit 15] Protected 0us DA:01:00:5e:00:00:6d SA:60:e3:27:0a:8c:38 BSSID:2a:d3:8a:17:b0:d8 Data IV:72d8c0 Pad 0 KeyID 0
qos    : 2022-11-28 17:15:24.917412 24.0 Mb/s [bit 15] Protected 0us CF +QoS DA:60:e3:27:12:a2:a7 SA:60:e3:27:0a:8c:38 BSSID:2a:d3:8a:17:b0:d8 Data IV:7ed8c0 Pad 0 KeyID 0
         2022-11-28 17:15:24.917412 24.0 Mb/s [bit 15] Protected 0us CF +QoS DA:60:e3:27:12:a2:a7 SA:60:e3:27:0a:8c:38 BSSID:2a:d3:8a:17:b0:d8 Data IV:7ed8c0 Pad 0 KeyID 0
ack    : 2022-11-28 17:15:24.918176 140856760us tsft 24.0 Mb/s 2462 MHz 11g -48dBm signal -48dBm signal antenna 0 0us RA:60:e3:27:0a:8c:38 Acknowledgment
rts    : 2022-11-28 17:15:24.927159 140866443us tsft 12.0 Mb/s 2462 MHz 11g -49dBm signal -49dBm signal antenna 0 204us RA:c4:e9:84:1d:a2:bc TA:60:e3:27:12:a2:a7 Request-To-Send
cts    : 2022-11-28 17:15:24.927161 140866496us tsft 12.0 Mb/s 2462 MHz 11g -34dBm signal -34dBm signal antenna 0 156us RA:60:e3:27:12:a2:a7 Clear-To-Send
qos_ant: 2022-11-28 17:15:24.918193 140856906us tsft 24.0 Mb/s 2462 MHz 11g -52dBm signal -52dBm signal antenna 0 Protected 44us CF +QoS DA:60:e3:27:12:a2:a7 SA:60:e3:27:10:b8:c6 BSSID:2a:d3:8a:17:b0:d8 Data IV:adc399 Pad 0 KeyID 0
qos_q_r: 2022-11-28 17:15:24.920586 140859270us tsft 24.0 Mb/s 2462 MHz 11g -48dBm signal -48dBm signal antenna 0 Retry Protected 44us CF +QoS DA:60:e3:27:12:a2:a7 SA:60:e3:27:10:8a:cf BSSID:2a:d3:8a:17:b0:d8 Data IV:9b1a60 Pad 0 KeyID 0
prob_r : 2022-11-28 17:15:25.232667 141171539us tsft 1.0 Mb/s 2462 MHz 11b -33dBm signal -33dBm signal antenna 0 Protected 314us DA:01:00:5e:00:00:6d SA:c4:e9:84:1d:a2:bc BSSID:2a:d3:8a:17:b0:d8 Data IV:17093c Pad 0 KeyID 0
prob_rp: 2022-11-28 17:15:30.347422 146288031us tsft 24.0 Mb/s 2462 MHz 11g -76dBm signal -76dBm signal antenna 0 44us BSSID:18:8b:45:06:af:80 DA:c4:e9:84:1d:a2:bc SA:18:8b:45:06:af:80 Probe Response (eduroam) [12.0 18.0 24.0* 36.0 48.0 54.0 Mbit] CH: 11, PRIVACY

"""

macs = {"60:e3:27:10:b8:c6": 1, "60:e3:27:0a:8c:38": 2, "60:e3:27:10:8a:cf": 3, "c4:e9:84:1d:a2:bc": 4,
        "60:e3:27:12:a2:a7": 5, "60:e3:27:10:c8:86": 6}

lookupword = ["take"] + list(macs.keys())  # add last MAC interface
excludewords = []  # ["Probe Response", "[bit 15]"]

bssid = ""

csvfile = open("rssi.csv", "w")
csvfile.write("mode,time,us,rate,rssi1,rssi2,da,sa,ra,ta,bssid,nodei\n")

nodei = 2


class Parameters:
    def __init__(self, modeID):
        self.mid = modeID
        self.example = ""
        self.size = 0


def check_match_len(i, line, param):
    data = line.split(" ")
    if param.size == 0:
        param.size = len(data)
        param.example = l
    else:
        if param.size != len(data):
            if param.size + 1 != len(data):
                print("match_len", i, line, "\nexample:", param.example)
                exit(1)


def MACaddress(macaddress):
    global macs

    if macaddress in macs.keys():
        return macs[macaddress]
    return macaddress


epoch = datetime.datetime.fromtimestamp(0)


def totimestamp(timeus):
    """
    17:15:03.575931
    :param timeus:
    :return:
    """
    global epoch

    dt = datetime.datetime.strptime(myDate, "%Y-%m-%d %H:%M:%S.%f")
    return (dt - epoch).total_seconds()


def match_beacon_tstf(line, param):
    #print(__name__)
    global csvfile
    data = line.split(" ")
    if param.size == 0:
        param.size = len(data)
    else:
        if param.size != len(data):
            print(line)


def match_beacon(line, param):
    global csvfile
    line.split(" ")


def match_cts(line, param):
    global csvfile

    data = line.split(" ")

    time = data[1]
    us = data[2]
    rate = data[4]  # Mbps, always?
    rssi1 = data[9]
    rssi2 = data[11]
    ra = data[16]

    da = ""
    sa = ""
    ta = ""
    bssid = ""

    ra = MACaddress(ra[3:])
    # time = totimestamp(time)
    # csvfile.write(
    #    "{},{},{},{},{},{},{},{},{},{}\n".format(param.mid, time, us, rate, rssi1, rssi2, da, sa, ra, ta, bssid))


def match_rts(line, param):
    global csvfile

    data = line.split(" ")

    time = data[1]
    us = data[2]
    rate = data[4]  # Mbps, always?
    rssi1 = data[9]
    rssi2 = data[11]
    t = data[15]
    ra = data[16]
    ta = data[17]

    da = ""
    sa = ""
    bssid = ""

    ra = MACaddress(ra[3:])
    ta = MACaddress(ta[3:])

    # csvfile.write(
    #    "{},{},{},{},{},{},{},{},{},{}\n".format(param.mid, time, us, rate, rssi1, rssi2, da, sa, ra, ta, bssid))


def match_ack(line, param):
    global csvfile
    global nodei

    data = line.split(" ")

    time = data[1]
    us = data[2]
    rate = data[4]  # Mbps, always?
    rssi1 = data[9]
    rssi2 = data[11]
    ra = data[16]

    da = ""
    sa = ""
    bssid = ""
    ta = ""

    ra = MACaddress(ra[3:])

    # csvfile.write(
    #    "{},{},{},{},{},{},{},{},{},{},{}\n".format(param.mid, time, us, rate, rssi1, rssi2, da, sa, ra, ta, bssid,nodei))


def match_qos(line, param):
    global csvfile


def match_qos_retry(line, param):
    global csvfile

    data = line.split(" ")

    time = data[1]
    us = data[2]
    rate = data[4]  # Mbps, always?
    rssi1 = data[9]
    rssi2 = data[11]
    t = data[17]
    ra = ""

    da = data[20]
    sa = data[21]
    bssid = data[22]
    ta = ""

    da = MACaddress(da[3:])
    sa = MACaddress(sa[3:])
    bssid = MACaddress(bssid[6:])

    # csvfile.write(
    #    "{},{},{},{},{},{},{},{},{},{}\n".format(param.mid, time, us, rate, rssi1, rssi2, da, sa, ra, ta, bssid))


def match_qos_ant(line, param):
    global csvfile
    global nodei

    data = line.split(" ")

    time = data[1]
    us = data[2]
    rate = data[4]  # Mbps, always?
    rssi1 = data[9]
    rssi2 = data[11]
    t = data[16]
    ra = ""

    da = data[19]
    sa = data[20]
    bssid = data[21]
    ta = ""

    da = MACaddress(da[3:])
    sa = MACaddress(sa[3:])
    bssid = MACaddress(bssid[6:])

    csvfile.write(
        "{},{},{},{},{},{},{},{},{},{},{},{}\n".format(param.mid, time, us, rate, rssi1, rssi2, da, sa, ra, ta, bssid,
                                                       nodei))


def match_bit15(line, param):
    global csvfile
    "2022-11-28 17:15:05.599418 1.0 Mb/s [bit 15] Protected 0us DA:01:00:5e:00:00:6d SA:60:e3:27:0a:8c:38 BSSID:2a:d3:8a:17:b0:d8 Data IV:72d8c0 Pad 0 KeyID 0"


def match_probe_request(line, param):
    global csvfile

    data = line.split(" ")

    time = data[1]
    us = data[2]
    rate = data[4]  # Mbps, always?
    rssi1 = data[9]
    rssi2 = data[11]
    t = data[16]
    ra = ""

    da = data[17]
    sa = data[18]
    bssid = data[19]
    ta = ""

    da = MACaddress(da[3:])
    sa = MACaddress(sa[3:])
    bssid = MACaddress(bssid[6:])

    # csvfile.write("{time},{us},{rate},{rssi1},{rssi2},{da},{sa},{ra},{ta},{bssid}\n")

    # csvfile.write(
    #    "{},{},{},{},{},{},{},{},{},{}\n".format(param.mid, time, us, rate, rssi1, rssi2, da, sa, ra, ta, bssid))


def match_probe_response(line, param):
    global csvfile


def match_probe_response_retry_privacy(line, param):
    global csvfile


def match_probe_response_privacy(line, param):
    global csvfile


def match_probe_response_retry(line, param):
    global csvfile


def match_other(line, param):
    global csvfile
    data = line.split(" ")

    time = data[1]
    us = data[2]
    rate = data[4]  # Mbps, always?
    rssi1 = data[9]
    rssi2 = data[11]
    da = data[17]
    sa = data[18]
    bssid = data[19]
    ra = ""
    ta = ""

    da = MACaddress(da[3:])
    sa = MACaddress(sa[3:])
    bssid = MACaddress(bssid[6:])

    # csvfile.write(
    #    "{},{},{},{},{},{},{},{},{},{}\n".format(param.mid, time, us, rate, rssi1, rssi2, da, sa, ra, ta, bssid))


matches = [[["Beacon", "tsft"], match_beacon_tstf, Parameters(0)],
           [["Beacon"], match_beacon, Parameters(1)],
           [["Clear-To-Send"], match_cts, Parameters(2)],
           [["Request-To-Send"], match_rts, Parameters(3)],
           [["Acknowledgment"], match_ack, Parameters(4)],
           [["+QoS", "antenna", "Retry"], match_qos_retry, Parameters(5)],
           [["+QoS", "antenna"], match_qos_ant, Parameters(6)],
           [["+QoS"], match_qos, Parameters(7)],
           [["bit 15"], match_bit15, Parameters(8)],
           [["Probe Request"], match_probe_request, Parameters(9)],
           [["Probe Response", "Retry", "PRIVACY"], match_probe_response_retry_privacy, Parameters(10)],
           [["Probe Response", "PRIVACY"], match_probe_response_privacy, Parameters(11)],
           [["Probe Response", "Retry"], match_probe_response_retry, Parameters(12)],
           [["Probe Response"], match_probe_response, Parameters(13)]
           ]

other = Parameters(14)

with open("X:/monitor.log", "r") as rf:
    lines = rf.readlines()
    for i, l in enumerate(lines):
        print(i)
        exclude = False
        for excludeword in excludewords:  # exclude word first
            if excludeword in l:
                exclude = True
                break
        if exclude:
            continue
        for lw in lookupword:
            if lw in l:
                for match in matches:
                    keys = match[0]
                    cond = True
                    for key in keys:
                        if key not in l:
                            cond = False
                            break
                    if cond:
                        match[1](l, match[2])
                        # check_match_len(i, l, match[2])
                        break
                else:
                    # print(i, l)
                    # check_match_len(i, l, other)
                    match_other(l, other)
                    # exit(0)
