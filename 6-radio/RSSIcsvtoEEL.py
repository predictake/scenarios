import random

random.seed(0)

folder = "C:/Users/Yann/Documents/MATLAB/"

fpathlossname = "pathloss.csv"
feelname = "laptop.eel"
fcsvname = "laptop.csv"

fpathloss = open(folder + fpathlossname, "r")
feel = open(folder + feelname, "w")
fcsv = open(folder + fcsvname, "w")  # "ts,d,i_nem_id,i_lat,i_lon,i_alt,j_nem_id,j_lat,j_lon,j_alt,pathloss\n"

lat = 46.7929139
lon = 7.1601193
alt = 100

# nandisconnectionpathloss = 150

laptops = 6

pathlosseel = "{0}  nem:{1} pathloss "
pathlosslink = "nem:{0},{1} "
locationeel = "{0}  nem:{1} location gps {2},{3},100\n"

links = [[0] * laptops for i in range(laptops)]
recalltime = None
t = 1
increment = 1  # matlab, 0.2 emane
posindexpathloss = 0

nodelocationdirection = [((random.randint(1, 10) - 5) / 100000, (random.randint(1, 10) - 5) / 100000) for i in
                         range(laptops)]
nodecurrentlocation = [[lon + nodelocationdirection[i][0], lat + nodelocationdirection[i][1]] for i in
                       range(laptops)]

pathlossnem = ""
if 0:  # old pathloss file
    for line in fpathloss.readlines():
        time, src, dest, pathloss = line.strip().split(",")

        if recalltime is None:
            recalltime = time
        if recalltime != time:  # verify each second all links 5*6 = 30, if not available set to a large pathloss
            recalltime = time
            locationnem = ""
            for i in range(laptops):
                locationnem += locationeel.format(t, i + 1, round(lon + (random.randint(1, 10) - 5) / 1000000, 7),
                                                  round(lat + (random.randint(1, 10) - 5) / 1000000, 7))
                pathlossnem = pathlosseel.format(t, i + 1)
                for j in range(laptops):
                    if i == j:
                        continue
                    if links[i][j] > 0:
                        pathlossnem += pathlosslink.format(j + 1, links[i][j])
                    else:
                        pathlossnem += pathlosslink.format(j + 1, nandisconnectionpathloss)
                        print("missing pathloss", t, i, j)
                feel.write(pathlossnem + "\n")
            feel.write(locationnem)

            links = [[0] * laptops for i in range(laptops)]
            t += 1

        links[int(src) - 1][int(dest) - 1] = float(pathloss)

# new pathloss file with consistent timestep for all links

else:
    for line in fpathloss.readlines():
        pathlosses = line.strip().split(
            ",")  # lRX_TX,start with link: 1_x, x_x-1. Since EMANE does not support in version 1.3.3 bidirectional links, only the first link is choosed between 1_x and x_1
        # nblinks = len(pathlosses)
        locationnem = ""
        for i in range(laptops):
            locationnem += locationeel.format(round(t, 1), i + 1, round(nodecurrentlocation[i][0], 7),
                                              round(nodecurrentlocation[i][1], 7))
            nodecurrentlocation[i][
                0] += 0  # do not move otherwise the slotovhead is not respected #nodelocationdirection[i][0]
            nodecurrentlocation[i][1] += 0  # nodelocationdirection[i][1]

            if i < laptops - 1:
                pathlossnem += pathlosseel.format(round(t, 1), i + 1)
                for j in range(laptops):
                    if i == j:
                        continue
                    if i < j:
                        pathlossnem += pathlosslink.format(j + 1, round(float(pathlosses[posindexpathloss]), 1))
                    posindexpathloss += 1

                pathlossnem += '\n'

        feel.write(locationnem)
        t += increment
        posindexpathloss = 0

    feel.write(pathlossnem)  # write pathloss at the end to be compatible with old scripts...

feel.close()
fpathloss.close()
